## Sommaire

- [TP2 : On va router des trucs](#tp2--on-va-router-des-trucs)
  - [Sommaire](#sommaire)
  - [0. Prérequis](#0-prérequis)
  - [I. ARP](#i-arp)
    - [1. Echange ARP](#1-echange-arp)
    - [2. Analyse de trames](#2-analyse-de-trames)
  - [II. Routage](#ii-routage)
    - [1. Mise en place du routage](#1-mise-en-place-du-routage)
    - [2. Analyse de trames](#2-analyse-de-trames-1)
    - [3. Accès internet](#3-accès-internet)
  - [III. DHCP](#iii-dhcp)
    - [1. Mise en place du serveur DHCP](#1-mise-en-place-du-serveur-dhcp)
    - [2. Analyse de trames](#2-analyse-de-trames-2)



Les différentes captures avec wireshark sont dans le dossier Captures_Wireshark .
## I. ARP
on va effectuer un ping pour chaque machine.
```
[paul@node1 ~]$ ping 10.2.1.12
PING 10.2.1.12 (10.2.1.12) 56(84) bytes of data.
64 bytes from 10.2.1.12: icmp_seq=1 ttl=64 time=0.384 ms
64 bytes from 10.2.1.12: icmp_seq=2 ttl=64 time=0.417 ms
64 bytes from 10.2.1.12: icmp_seq=3 ttl=64 time=0.387 ms
^C
--- 10.2.1.12 ping statistics ---
3 packets transmitted, 3 received, 0% packet loss, time 2029ms
rtt min/avg/max/mdev = 0.384/0.396/0.417/0.014 ms
```
```
[paul@node2 ~]$ ping 10.2.1.11
PING 10.2.1.11 (10.2.1.11) 56(84) bytes of data.
64 bytes from 10.2.1.11: icmp_seq=1 ttl=64 time=0.444 ms
64 bytes from 10.2.1.11: icmp_seq=2 ttl=64 time=0.396 ms
64 bytes from 10.2.1.11: icmp_seq=3 ttl=64 time=0.416 ms
64 bytes from 10.2.1.11: icmp_seq=4 ttl=64 time=0.468 ms
64 bytes from 10.2.1.11: icmp_seq=5 ttl=64 time=0.469 ms
^C
--- 10.2.1.11 ping statistics ---
5 packets transmitted, 5 received, 0% packet loss, time 4088ms
rtt min/avg/max/mdev = 0.396/0.438/0.469/0.036 ms
```

arp des deux machines : 
```
[paul@node1 ~]$ arp -a
? (10.2.1.1) at 0a:00:27:00:00:10 [ether] on enp0s8
? (10.2.1.12) at 08:00:27:19:03:47 [ether] on enp0s8
```
```
[paul@node2 ~]$ arp -a
? (10.2.1.1) at 0a:00:27:00:00:10 [ether] on enp0s8
? (10.2.1.11) at 08:00:27:53:88:87 [ether] on enp0s8
```
adresse mac de node1 depuis node2: ```08:00:27:53:88:87```

adresse mac de node2 depuis node1: ```08:00:27:19:03:47```

arp de node1 : 
```
[paul@node1 ~]$ arp
Address                  HWtype  HWaddress           Flags Mask            Iface
10.2.1.1                 ether   0a:00:27:00:00:10   C                     enp0s8
10.2.1.12                ether   08:00:27:19:03:47   C                     enp0s8
```
il nous faut donc ```08:00:27:19:03:47``` comme adresse mac pour node2.

pour voir l'adresse MAC de node2 , on fait un  ```ip a``` et on regarde à enp0s8 et link/ether.

```
[paul@node2 ~]$ ip a
[...]
2: enp0s8: 
[...]
link/ether 08:00:27:19:03:47 brd ff:ff:ff:ff:ff:ff
inet 10.2.1.12/24 brd 
[...]
```
on remarque que l'adresse mac de node2 soit 08:00:27:19:03:47 est bien la meme que ce qu'on a eu avant .
    
  ### 2. Analyse de trames
  
```  
[paul@node2 ~]$ sudo ip neigh flush all
  
[paul@node2 ~]$ sudo tcpdump -i enp0s8 -c 10 -w mon_fichier.pcap not port 22
dropped privs to tcpdump
tcpdump: listening on enp0s8, link-type EN10MB (Ethernet), capture size 262144 bytes
10 packets captured
11 packets received by filter
0 packets dropped by kernel
```
```
[paul@node1 ~]$ sudo ip neigh flush all
[paul@node1 ~]$ ping 10.2.1.12
PING 10.2.1.12 (10.2.1.12) 56(84) bytes of data.
64 bytes from 10.2.1.12: icmp_seq=1 ttl=64 time=2.12 ms
64 bytes from 10.2.1.12: icmp_seq=2 ttl=64 time=2.52 ms
```

| ordre | type trame  | source                   | destination                |
|-------|-------------|--------------------------|----------------------------|
| 1     | Requête ARP | `node1``08:00:27:53:88:87`| Broadcast `00:00:00:00:00:00`|
| 2     | Réponse ARP | `node2``08:00:27:19:03:47`| `node1` `08:00:27:53:88:87`  |
| ...   | ...         | ...                      | ...                        |

📁 Capture réseau tp2_arp.pcap
## II. Routage

### 1. Mise en place du routage

Activer le routage sur le noeud router.net2.tp2

```
[paul@router ~]$ sudo firewall-cmd --list-all
[sudo] password for paul:
public (active)
  target: default
  icmp-block-inversion: no
  interfaces: enp0s3 enp0s8 enp0s9
  sources:
  services: cockpit dhcpv6-client ssh
  ports:
  protocols:
  masquerade: no
  forward-ports:
  source-ports:
  icmp-blocks:
  rich rules:
[paul@router ~]$ sudo firewall-cmd --get-active-zone
public
  interfaces: enp0s3 enp0s8 enp0s9
[paul@router ~]$ sudo firewall-cmd --add-masquerade --zone=public
success
[paul@router ~]$ sudo firewall-cmd --add-masquerade --zone=public --permanent
success
[paul@router ~]$
```
Ajouter les routes statiques nécessaires pour que node1.net1.tp2 et marcel.net2.tp2 puissent se ping

pour node1 : 
```
[paul@node1 ~]$ sudo ip route add 10.2.2.0/24 via 10.2.1.254 dev enp0s8
[paul@node1 ~]$ sudo cat /etc/sysconfig/network-scripts/route-enp0s8
10.2.2.0/24 via 10.2.1.254 dev enp0s8
```
pour marcel : 
```
[paul@marcel ~]$ sudo ip route add 10.2.1.0/24 via 10.2.2.254 dev enp0s8
[paul@marcel ~]$  sudo cat /etc/sysconfig/network-scripts/route-enp0s8
10.2.1.0/24 via 10.2.2.254 dev enp0s8
```
le fameux ping : 

```
[paul@marcel ~]$ ping -c 2 10.2.1.11
PING 10.2.1.11 (10.2.1.11) 56(84) bytes of data.
64 bytes from 10.2.1.11: icmp_seq=1 ttl=63 time=1.48 ms
64 bytes from 10.2.1.11: icmp_seq=2 ttl=63 time=1.29 ms

--- 10.2.1.11 ping statistics ---
2 packets transmitted, 2 received, 0% packet loss, time 1002ms
rtt min/avg/max/mdev = 1.292/1.383/1.475/0.098 ms
```

### 2. Analyse de trames

Analyse des échanges ARP

on vide les tables ARP : 
```
[paul@node1 ~]$ sudo ip neigh flush all
```
```
[paul@router ~]$ sudo ip neigh flush all
```
```
[paul@marcel ~]$ sudo ip neigh flush all
```

ping node1 vers marcel : 
```
[paul@node1 ~]$ ping -c 2 10.2.2.12
PING 10.2.2.12 (10.2.2.12) 56(84) bytes of data.
64 bytes from 10.2.2.12: icmp_seq=1 ttl=63 time=2.18 ms
64 bytes from 10.2.2.12: icmp_seq=2 ttl=63 time=1.10 ms

--- 10.2.2.12 ping statistics ---
2 packets transmitted, 2 received, 0% packet loss, time 1002ms
rtt min/avg/max/mdev = 1.096/1.635/2.175/0.541 ms
```
tables arp des 3 noeuds : 
```
[paul@router ~]$ arp -a
? (10.2.2.12) at 08:00:27:d3:7f:18 [ether] on enp0s9
? (10.2.1.1) at 0a:00:27:00:00:11 [ether] on enp0s8
? (10.2.1.11) at 08:00:27:53:88:87 [ether] on enp0s8
? (10.2.1.11) at 08:00:27:53:88:87 [ether] on enp0s8
```
```
[paul@node1 ~]$ arp -a
? (10.2.1.254) at 08:00:27:4a:57:df [ether] on enp0s8
? (10.2.1.1) at 0a:00:27:00:00:11 [ether] on enp0s8
```
```
[paul@marcel ~]$ arp -a
? (10.2.2.254) at 08:00:27:c4:42:4d [ether] on enp0s8
? (10.2.2.1) at 0a:00:27:00:00:12 [ether] on enp0s8
```
j'en déduis que : 
node1 demande à routeur comment le rejoindre  et ensuite routeur demande à  marcel comment le rejoindre.

( Je viens de reprendre le tp et je viens de voir que tu as changé le tp et qu'on a besoin que de node1 et marcel pour les .pcap donc je ne mettrai pas celle du routeur)

Depuis node1 : 
```
[paul@node1 ~]$ sudo ip neigh flush all
[sudo] password for paul:
[paul@node1 ~]$ sudo tcpdump -i enp0s8 -c 10 -w tp2_routage_node1.pcap not port 22
[sudo] password for paul:
dropped privs to tcpdump
tcpdump: listening on enp0s8, link-type EN10MB (Ethernet), capture size 262144 bytes
10 packets captured
10 packets received by filter
0 packets dropped by kernel
```
![](./img/test1.png) 

ping effectué depuis la vm node1

Depuis marcel : 
```
[paul@marcel ~]$ sudo ip neigh flush all
[sudo] password for paul:
[paul@marcel ~]$ sudo tcpdump -i enp0s8 -c 10 -w tp2_routage_marcel.pcap not port 22
[sudo] password for paul:
dropped privs to tcpdump
tcpdump: listening on enp0s8, link-type EN10MB (Ethernet), capture size 262144 bytes
10 packets captured
11 packets received by filter
0 packets dropped by kernel
```
Ensuite je recupère les fichiers :
```
PS C:\Users\Paula> scp paul@10.2.1.11:/home/paul/tp2_routage_node1.pcap ./tp2_routage_node1.pcap
paul@10.2.1.11's password:
tp2_routage_node1.pcap                                                                100% 1070     1.0MB/s   00:00
PS C:\Users\Paula> scp paul@10.2.2.12:/home/paul/tp2_routage_marcel.pcap ./tp2_routage_marcel.pcap
paul@10.2.2.12's password:
tp2_routage_marcel.pcap                                                               100% 1070   545.9KB/s   00:00
```



| ordre | type trame  | IP source   | MAC source                    | IP destination  | MAC destination                   |
|-------|-------------|-------------|-------------------------------|-----------------|-----------------------------------|
| 1     | Requête ARP | x           | `node1` `08:00:27:53:88:87`   | x               | Broadcast `FF:FF:FF:FF:FF`        |
| 2     | Réponse ARP | x           | `router` `08:00:27:4a:57:df`  | x               | `node1` `08:00:27:53:88:87`       |
| ...   | ...         | ...         | ...                           |                 |                                   |
| ?     | Ping        | '10.2.1.11' | 'node1' '08:00:27:53:88:87'   | '10.2.2.12'     | router  '08:00:27:4a:57:df'       |
| 2     | Requête ARP | x           | `router` `08:00:27:c4:42:4d`  | x               | `Broadcast `FF:FF:FF:FF:FF`       |
| 2     | Réponse ARP | x           | `marcel` `08:00:27:d3:7f:18`  | x               | `router` `08:00:27:c4:42:4d       |
| ?     | Pong        | '10.2.2.12' |`router` `08:00:27:4a:57:df`   | `10.2.1.11`     | `node1` `08:00:27:53:88:87`       |

📁 Capture réseau tp2_routage_node1.pcap tp2_routage_marcel.pcap
### 3. Accès internet

#### Donnez un accès internet à vos machines

node1 : 

``` [paul@node1 network-scripts]$ sudo nmcli con reload
[paul@node1 network-scripts]$ sudo nmcli con up enp0s8
Connection successfully activated (D-Bus active path: /org/freedesktop/NetworkManager/ActiveConnection/2)
[paul@node1 network-scripts]$ ping 8.8.8.8
PING 8.8.8.8 (8.8.8.8) 56(84) bytes of data.
64 bytes from 8.8.8.8: icmp_seq=1 ttl=114 time=18.8 ms
64 bytes from 8.8.8.8: icmp_seq=2 ttl=114 time=26.1 ms
64 bytes from 8.8.8.8: icmp_seq=3 ttl=114 time=17.9 ms
64 bytes from 8.8.8.8: icmp_seq=4 ttl=114 time=21.8 ms
^C
--- 8.8.8.8 ping statistics ---
4 packets transmitted, 4 received, 0% packet loss, time 3007ms
rtt min/avg/max/mdev = 17.919/21.161/26.097/3.197 ms
[paul@node1 network-scripts]$ ip route show
default via 10.2.1.254 dev enp0s8 proto static metric 100
```


marcel : 

``` 
[paul@marcel ~]$ sudo nano /etc/sysconfig/network-scripts/ifcfg-enp0s8
[paul@marcel ~]$ [paul@marcel ~]$ cd
[paul@marcel ~]$ ping 8.8.8.8
connect: Network is unreachable
[paul@marcel ~]$ sudo nmcli con reload
[paul@marcel ~]$ sudo nmcli con up enp0s8
Connection successfully activated (D-Bus active path: /org/freedesktop/NetworkManager/ActiveConnection/2)
[paul@marcel ~]$ ping 8.8.8.8
PING 8.8.8.8 (8.8.8.8) 56(84) bytes of data.
64 bytes from 8.8.8.8: icmp_seq=1 ttl=114 time=20.2 ms
64 bytes from 8.8.8.8: icmp_seq=2 ttl=114 time=18.4 ms
64 bytes from 8.8.8.8: icmp_seq=3 ttl=114 time=18.10 ms
^C
--- 8.8.8.8 ping statistics ---
3 packets transmitted, 3 received, 0% packet loss, time 2003ms
rtt min/avg/max/mdev = 18.383/19.202/20.245/0.792 ms
```

j'ai mis en DNS1=1.1.1.1 et cela foncctionne, 

```[paul@node1 network-scripts]$ dig gitlab.com

; <<>> DiG 9.11.26-RedHat-9.11.26-4.el8_4 <<>> gitlab.com
;; global options: +cmd
;; Got answer:
;; ->>HEADER<<- opcode: QUERY, status: NOERROR, id: 7716
;; flags: qr rd ra; QUERY: 1, ANSWER: 1, AUTHORITY: 0, ADDITIONAL: 1

;; OPT PSEUDOSECTION:
; EDNS: version: 0, flags:; udp: 1232
;; QUESTION SECTION:
;gitlab.com.                    IN      A

;; ANSWER SECTION:
gitlab.com.             105     IN      A       172.65.251.78

;; Query time: 20 msec
;; SERVER: 1.1.1.1#53(1.1.1.1)
;; WHEN: Sun Sep 26 19:27:38 CEST 2021
;; MSG SIZE  rcvd: 55
```
```[paul@marcel ~]$ dig ynov.com

; <<>> DiG 9.11.26-RedHat-9.11.26-4.el8_4 <<>> ynov.com
;; global options: +cmd
;; Got answer:
;; ->>HEADER<<- opcode: QUERY, status: NOERROR, id: 46069
;; flags: qr rd ra; QUERY: 1, ANSWER: 1, AUTHORITY: 0, ADDITIONAL: 1

;; OPT PSEUDOSECTION:
; EDNS: version: 0, flags:; udp: 1232
; COOKIE: 5c936b538fb5bb2671036a846150b0b78b1b2aeb8f72d9c6 (good)
;; QUESTION SECTION:
;ynov.com.                      IN      A

;; ANSWER SECTION:
ynov.com.               10691   IN      A       92.243.16.143

;; Query time: 20 msec
;; SERVER: 192.168.1.1#53(192.168.1.1)
;; WHEN: Sun Sep 26 19:41:08 CEST 2021
;; MSG SIZE  rcvd: 81
```
Je vais maintenant essayer un ping vers un nom de domaine tel que google.com

```[paul@node1 network-scripts]$ ping google.com
PING google.com (172.217.18.206) 56(84) bytes of data.
64 bytes from par10s38-in-f14.1e100.net (172.217.18.206): icmp_seq=1 ttl=113 time=17.2 ms
64 bytes from par10s38-in-f14.1e100.net (172.217.18.206): icmp_seq=2 ttl=113 time=17.6 ms
^X64 bytes from par10s38-in-f14.1e100.net (172.217.18.206): icmp_seq=3 ttl=113 time=17.3 ms
^C
--- google.com ping statistics ---
3 packets transmitted, 3 received, 0% packet loss, time 2004ms
rtt min/avg/max/mdev = 17.241/17.365/17.555/0.204 ms
```
```
[paul@marcel ~]$ ping google.com
PING google.com (142.250.178.142) 56(84) bytes of data.
64 bytes from par21s22-in-f14.1e100.net (142.250.178.142): icmp_seq=1 ttl=114 time=18.3 ms
64 bytes from par21s22-in-f14.1e100.net (142.250.178.142): icmp_seq=2 ttl=114 time=26.4 ms
^C
--- google.com ping statistics ---
2 packets transmitted, 2 received, 0% packet loss, time 1002ms
rtt min/avg/max/mdev = 18.263/22.352/26.441/4.089 ms
```
```

Tout fonctionne !

#### Analyse de trames

```[paul@node1 ~]$ sudo ip neigh flush all
[sudo] password for paul:
[paul@node1 ~]$ sudo tcpdump -i enp0s8 -c 10 -w tp2_routage_internet.pcap not port 22
dropped privs to tcpdump
tcpdump: listening on enp0s8, link-type EN10MB (Ethernet), capture size 262144 bytes
ping 8.8.8.8
10 packets captured
10 packets received by filter
0 packets dropped by kernel
[paul@node1 ~]$ ping 8.8.8.8
PING 8.8.8.8 (8.8.8.8) 56(84) bytes of data.
64 bytes from 8.8.8.8: icmp_seq=1 ttl=114 time=26.5 ms
64 bytes from 8.8.8.8: icmp_seq=2 ttl=114 time=26.6 ms
64 bytes from 8.8.8.8: icmp_seq=3 ttl=114 time=23.1 ms
```

| ordre | type trame  | IP source   | MAC source                    | IP destination  | MAC destination                   |
|-------|-------------|-------------|-------------------------------|-----------------|-----------------------------------|
| 1     | Ping        | 10.2.1.11   | `node1` `08:00:27:53:88:87`   | 8.8.8.8         | router   08:00:27:4a:57:df        |
| 2     | Pong        | 8.8.8.8     | `router` `08:00:27:4a:57:df`  | 10.2.1.11       | `node1` `08:00:27:53:88:87`       |

📁 Capture réseau tp2_routage_internet.pcap
## III. DHCP

### 1. Mise en place du serveur DHCP

#### Sur la machine node1.net1.tp2, vous installerez et configurerez un serveur DHCP

``` [paul@node1 ~]$ sudo dnf -y install dhcp-server ```

```[paul@node1 ~]$ sudo nano /etc/dhcp/dhcpd.conf```

puis je vais mettre ces valeurs : 

```
default-lease-time 900;
max-lease-time 10800;
ddns-update-style none;
authoritative;
subnet 10.2.1.0 netmask 255.255.255.0 {
  range 10.2.1.10 10.2.1.200;
  option routers 10.2.1.254;
  option subnet-mask 255.255.255.0;
  option domain-name-servers 1.1.1.1;

}
```

```
[paul@node1 ~]$ systemctl enable --now dhcpd 
```

```
[paul@node1 ~]$ sudo firewall-cmd --add-service=dhcp
[sudo] password for paul:
success
[paul@node1 ~]$ sudo firewall-cmd --runtime-to-permanent
success
[paul@node1 ~]$
```

```
[paul@node1 ~]$ ll /var/lib/dhcpd
total 8
-rw-r--r--. 1 dhcpd dhcpd   0 Jun 11 02:13 dhcpd6.leases
-rw-r--r--. 1 dhcpd dhcpd 283 Sep 26 21:50 dhcpd.leases
-rw-r--r--. 1 dhcpd dhcpd 219 Sep 26 21:42 dhcpd.leases~
```

```
[paul@node1 ~]$ cat /var/lib/dhcpd/dhcpd.leases
# The format of this file is documented in the dhcpd.leases(5) manual page.
# This lease file was written by isc-dhcp-4.3.6

# authoring-byte-order entry is generated, DO NOT DELETE
authoring-byte-order little-endian;

server-duid "\000\001\000\001(\343\213\201\010\000'S\210\207";
```

```
[paul@node2 ~]$ sudo dhclient
```

#### Améliorer la configuration du DHCP

Vérifions les résultat :

```
[paul@node2 ~]$ [paul@node2 ~]$ ping -c 1 10.2.1.254
PING 10.2.1.254 (10.2.1.254) 56(84) bytes of data.
64 bytes from 10.2.1.254: icmp_seq=1 ttl=64 time=0.534 ms

--- 10.2.1.254 ping statistics ---
1 packets transmitted, 1 received, 0% packet loss, time 0ms
rtt min/avg/max/mdev = 0.534/0.534/0.534/0.000 ms
[paul@node2 ~]$ ip route show
default via 10.2.1.254 dev enp0s8
default via 10.2.1.254 dev enp0s8 proto dhcp metric 100
10.2.1.0/24 dev enp0s8 proto kernel scope link src 10.2.1.12 metric 100
[paul@node2 ~]$ ping -c 1 8.8.8.8
PING 8.8.8.8 (8.8.8.8) 56(84) bytes of data.
64 bytes from 8.8.8.8: icmp_seq=1 ttl=114 time=19.0 ms

--- 8.8.8.8 ping statistics ---
1 packets transmitted, 1 received, 0% packet loss, time 0ms
rtt min/avg/max/mdev = 19.017/19.017/19.017/0.000 ms
[paul@node2 ~]$ dig google.com

; <<>> DiG 9.11.26-RedHat-9.11.26-4.el8_4 <<>> google.com
;; global options: +cmd
;; Got answer:
;; ->>HEADER<<- opcode: QUERY, status: NOERROR, id: 56168
;; flags: qr rd ra; QUERY: 1, ANSWER: 1, AUTHORITY: 0, ADDITIONAL: 1

;; OPT PSEUDOSECTION:
; EDNS: version: 0, flags:; udp: 1232
;; QUESTION SECTION:
;google.com.                    IN      A

;; ANSWER SECTION:
google.com.             239     IN      A       216.58.198.206

;; Query time: 20 msec
;; SERVER: 1.1.1.1#53(1.1.1.1)
;; WHEN: Sun Sep 26 23:08:10 CEST 2021
;; MSG SIZE  rcvd: 55
[paul@node2 ~]$ ping -c 1 ynov.com
PING ynov.com (92.243.16.143) 56(84) bytes of data.
64 bytes from xvm-16-143.dc0.ghst.net (92.243.16.143): icmp_seq=1 ttl=50 time=17.3 ms

--- ynov.com ping statistics ---
1 packets transmitted, 1 received, 0% packet loss, time 0ms
rtt min/avg/max/mdev = 17.268/17.268/17.268/0.000 ms
[paul@node2 ~]$
```

### 2. Analyse de trames

#### Analyse de trames

``` 
[paul@node1 ~]$ sudo ip neigh flush all
```

```
[paul@node2 ~]$ sudo ip neigh flush all
```
