# B2 réseau 2021-TP1
## Abeille Paul-Antoine pour la partie en solo et accompagné de Lucas Fallous pour la partie en duo.
### I.Exploration locale en solo
#### 1. Affichage d'informations sur la pile TCP/IP locale

#### Affichez les infos des cartes réseau de votre PC
IPCONFIG /all
```
Carte réseau sans fil Wi-Fi :

   Suffixe DNS propre à la connexion. . . : auvence.co
   Description. . . . . . . . . . . . . . : Intel(R) Wi-Fi 6 AX201 160MHz
   Adresse physique . . . . . . . . . . . : A8-7E-EA-46-E7-8B
   DHCP activé. . . . . . . . . . . . . . : Oui
   Configuration automatique activée. . . : Oui
   Adresse IPv6 de liaison locale. . . . .: fe80::50ea:ec35:890f:4042%12(préféré)
   Adresse IPv4. . . . . . . . . . . . . .: 10.33.2.79(préféré)
   Masque de sous-réseau. . . . . . . . . : 255.255.252.0
   Bail obtenu. . . . . . . . . . . . . . : lundi 13 septembre 2021 14:00:41
   Bail expirant. . . . . . . . . . . . . : lundi 13 septembre 2021 17:00:41
   Passerelle par défaut. . . . . . . . . : 10.33.3.253
   Serveur DHCP . . . . . . . . . . . . . : 10.33.3.254
   IAID DHCPv6 . . . . . . . . . . . : 111705834
   DUID de client DHCPv6. . . . . . . . : 00-01-00-01-26-96-1F-6D-7C-8A-E1-43-30-B5
   Serveurs DNS. . .  . . . . . . . . . . : 10.33.10.2
                                       10.33.10.148
                                       10.33.10.155
   NetBIOS sur Tcpip. . . . . . . . . . . : Activé 
   
   ```
   ```
   Carte Ethernet Ethernet :

   Statut du média. . . . . . . . . . . . : Média déconnecté
   Suffixe DNS propre à la connexion. . . : home
   Description. . . . . . . . . . . . . . : Realtek PCIe GbE Family Controller
   Adresse physique . . . . . . . . . . . : 7C-8A-E1-43-30-B5
   DHCP activé. . . . . . . . . . . . . . : Oui
   Configuration automatique activée. . . : Oui
   ```
   #### Affichez votre gateway
   
   ipconfig /all
   ```
   Passerelle par défaut. . . . . . . . . : 10.33.3.253
   ```
   #### Trouvez comment afficher les informations sur une carte IP (change selon l'OS)
   Sur Windows 10 tapez ,dans la barre de recherche en bas à gauche , : informations système, allez dans composants, ensuite réseau et enfin Carte. On peut y trouver toutes les infos nécessaires.
   ```
   Nom	[00000001] Intel(R) Wi-Fi 6 AX201 160MHz
   Adresse IP	10.33.2.79, fe80::50ea:ec35:890f:4042
   Adresse MAC	‪A8:7E:EA:46:E7:8B
   Passerelle IP par défaut	10.33.3.253
```
‬

#### à quoi sert la gateway dans le réseau d'YNOV ?
 La gateway dans le réseau YNOV nous permet d'être tous sur le meme réseau ynov et ensuite c'est la gateway qui se connecte au réseau internet. En gros elle permet de connecter le réseau ynov à "internet" avec une sorte de pont.
 
 #### 2. Modifications des informations
 #### A. Modification d'adresse IP (part 1)

-Utilisez l'interface graphique de votre OS pour changer d'adresse IP

sur W10 , il faut aller dans les paramètres systèmes , cherchez état du réseau et ensuite allez dans les propriétés du réseau actuel et mettre les paramétres ip sur manuel à la place d'automatique et ensuite entrez les valeurs.
ex: 10.33.2.79 -> 10.33.2.187
![](https://i.imgur.com/NvL4kLr.png)
![](https://i.imgur.com/nj4noX4.png)

 
 Pour ma part j'ai perdu mon accès internet un court instant, le temps que mon pc se reconnecte .Mais si le pc n'a toujours pas accès à internet cela veut dire qu'il y a déjà quelqu'un connecté au réseau avec cette adresse IP.
 ### B. Table ARP
 -Exploration de la table ARP
 
 Pour afficher la table ARP sur W10:
 ```
 arp -a
 ```
 résultat : 
 
 ```
 C:\Users\Paula>arp -a

[...]
  
  Interface : 10.33.2.79 --- 0xc
  Adresse Internet      Adresse physique      Type
  10.33.1.93            b8-9a-2a-3d-c1-1a     dynamique
  10.33.3.253           00-12-00-40-4c-bf     dynamique
  10.33.3.255           ff-ff-ff-ff-ff-ff     statique
  224.0.0.22            01-00-5e-00-00-16     statique
  224.0.0.251           01-00-5e-00-00-fb     statique
  224.0.0.252           01-00-5e-00-00-fc     statique
  239.255.255.250       01-00-5e-7f-ff-fa     statique
  255.255.255.255       ff-ff-ff-ff-ff-ff     statique
  [...]
  ```
  la passerelle IP : 10.33.3.253 -> adresse mac : 00-12-00-40-4c-bf
  
  j'ai simplement repris l'adresse IP de la passerelle cité précédemment, je l'ai ensuite retrouvé dans le tableau ARP et identifié son adresse MAC.
  
  -Et si on remplissait un peu la table ?

  ping : 


 ``` 
  C:\Users\Paula>ping 10.33.3.17

Envoi d’une requête 'Ping'  10.33.2.222 avec 32 octets de données :
Réponse de 10.33.2.222 : octets=32 temps=517 ms TTL=64
Réponse de 10.33.2.222 : octets=32 temps=309 ms TTL=64
Réponse de 10.33.2.222 : octets=32 temps=26 ms TTL=64
Réponse de 10.33.2.222 : octets=32 temps=320 ms TTL=64

Statistiques Ping pour 10.33.2.222:
    Paquets : envoyés = 4, reçus = 4, perdus = 0 (perte 0%),
Durée approximative des boucles en millisecondes :
    Minimum = 26ms, Maximum = 517ms, Moyenne = 293ms
PS C:\WINDOWS\system32>

C:\Users\Paula>ping 10.33.3.219

Envoi d’une requête 'Ping'  10.33.3.219 avec 32 octets de données :
Réponse de 10.33.3.219 : octets=32 temps=99 ms TTL=64
Réponse de 10.33.3.219 : octets=32 temps=99 ms TTL=64
Réponse de 10.33.3.219 : octets=32 temps=115 ms TTL=64
Réponse de 10.33.3.219 : octets=32 temps=132 ms TTL=64

Statistiques Ping pour 10.33.3.219:
    Paquets : envoyés = 4, reçus = 4, perdus = 0 (perte 0%),
Durée approximative des boucles en millisecondes :
    Minimum = 99ms, Maximum = 132ms, Moyenne = 111ms

C:\Users\Paula>ping 10.33.3.232

Envoi d’une requête 'Ping'  10.33.3.232 avec 32 octets de données :
Réponse de 10.33.3.232 : octets=32 temps=399 ms TTL=64
Réponse de 10.33.3.232 : octets=32 temps=411 ms TTL=64
Réponse de 10.33.3.232 : octets=32 temps=6 ms TTL=64
Réponse de 10.33.3.232 : octets=32 temps=16 ms TTL=64

Statistiques Ping pour 10.33.3.232:
    Paquets : envoyés = 4, reçus = 4, perdus = 0 (perte 0%),
Durée approximative des boucles en millisecondes :
    Minimum = 6ms, Maximum = 411ms, Moyenne = 208ms
```


    ensuite je fais arp -a et j'obtiens ça :
    
   ``` 
 Interface : 10.33.2.81 --- 0xc
  Adresse Internet      Adresse physique      Type
  10.33.2.190           74-4c-a1-d8-e6-03     dynamique
  10.33.2.193           e0-2b-e9-7d-a6-c2     dynamique
  10.33.2.222           88-bf-e4-cb-d9-46     dynamique
  10.33.2.255           ff-ff-ff-ff-ff-ff     statique
  10.33.3.219           a0-78-17-b5-63-bb     dynamique
  10.33.3.232           88-66-5a-4d-a7-f5     dynamique
  10.33.3.253           00-12-00-40-4c-bf     dynamique
 ```
 en premier c'est les IP et au milieu les adresses MAC .
 
### C. nmap 

-Utilisez nmap pour scanner le réseau de votre carte WiFi et trouver une adresse IP libre

 ![](https://i.imgur.com/19XnwBw.png)

![](https://i.imgur.com/nnFMvD1.png)
```
PS C:\WINDOWS\system32> nmap -sP 10.33.0.0/22
Starting Nmap 7.92 ( https://nmap.org ) at 2021-09-16 15:17 Paris, Madrid (heure dÆÚtÚ)
Nmap scan report for 10.33.0.10
Host is up (0.61s latency).
MAC Address: B0:6F:E0:4C:CF:EA (Samsung Electronics)
Nmap scan report for 10.33.0.13
Host is up (0.011s latency).
MAC Address: D2:50:0E:26:94:8B (Unknown)
Nmap scan report for 10.33.0.27
Host is up (0.018s latency).
MAC Address: A8:64:F1:8B:1D:4D (Intel Corporate)
Nmap scan report for 10.33.0.31
[...]
MAC Address: 00:0E:C4:C D:74:F5 (Iskra Transmission d.d.)
Nmap scan report for 10.33.2.79
Host is up.
Nmap done: 1024 IP addresses (141 hosts up) scanned in 41.55 seconds
```
#### table arp
```
PS C:\WINDOWS\system32> arp -a

Interface : 10.33.2.79 --- 0xc
  Adresse Internet      Adresse physique      Type
  10.33.0.67            dc-f5-05-db-cc-ab     dynamique
  10.33.0.85            9e-08-36-e0-c5-22     dynamique
  10.33.0.195           12-b9-05-11-2c-54     dynamique
  10.33.1.185           2a-eb-09-09-c0-1a     dynamique
  10.33.2.42            16-d8-79-91-57-a4     dynamique
  10.33.2.63            72-a3-54-59-fe-ec     dynamique
  10.33.2.230           98-46-0a-96-d5-22     dynamique
  10.33.2.237           be-fd-16-82-f4-05     dynamique
  10.33.3.13            26-7b-3f-46-6d-9e     dynamique
  10.33.3.179           94-e7-0b-0a-46-aa     dynamique
  10.33.3.219           a0-78-17-b5-63-bb     dynamique
  10.33.3.250           14-f6-d8-43-45-95     dynamique
  10.33.3.253           00-12-00-40-4c-bf     dynamique
  10.33.3.255           ff-ff-ff-ff-ff-ff     statique
  224.0.0.22            01-00-5e-00-00-16     statique
  224.0.0.251           01-00-5e-00-00-fb     statique
  224.0.0.252           01-00-5e-00-00-fc     statique
  239.255.255.250       01-00-5e-7f-ff-fa     statique
  
```PS C:\WINDOWS\system32> Get-NetNeighbor```

```
```
ifIndex IPAddress                                          LinkLayerAddress      State       PolicyStore
------- ---------                                          ----------------      -----       -----------
12      10.33.3.220                                        00-00-00-00-00-00     Unreachable ActiveStore
12      10.33.3.219                                        A0-78-17-B5-63-BB     Stale       ActiveStore
12      10.33.3.218                                        00-00-00-00-00-00     Unreachable ActiveStore
12      10.33.3.217                                        00-00-00-00-00-00     Unreachable ActiveStore
12      10.33.3.216                                        00-00-00-00-00-00     Unreachable ActiveStore
12      10.33.3.215                                        00-00-00-00-00-00     Unreachable ActiveStore
12      10.33.3.214                                        00-00-00-00-00-00     Unreachable ActiveStore
```

on peut voir que l'IP 10.33.3.217 est libre donc je vais l'utiliser.

![](https://i.imgur.com/Neti9Yt.png)
![](https://i.imgur.com/nOvUTxB.png)

commande nmap sur le réseau ynov : 
``` 
nmap -sP 10.33.0.0/22 
```

``` 
Starting Nmap 7.92 ( https://nmap.org ) at 2021-09-16 15:42 Paris, Madrid (heure dÆÚtÚ)
Nmap scan report for 10.33.0.8
Host is up (0.036s latency).
Nmap scan report for 10.33.0.10
Host is up (0.53s latency).
[...]
Nmap scan report for 10.33.3.253
Host is up (0.0090s latency).
MAC Address: 00:12:00:40:4C:BF (Cisco Systems)
Nmap scan report for 10.33.3.254
Host is up (0.0080s latency).
MAC Address: 00:0E:C4:CD:74:F5 (Iskra Transmission d.d.)
Nmap scan report for 10.33.3.217
Host is up.
Nmap done: 1024 IP addresses (88 hosts up) scanned in 85.88 seconds
```

![](https://i.imgur.com/3ChHwpX.png)

```
ipconfig 
Carte réseau sans fil Wi-Fi :

   Suffixe DNS propre à la connexion. . . :
   Adresse IPv6 de liaison locale. . . . .: fe80::50ea:ec35:890f:4042%12
   Adresse IPv4. . . . . . . . . . . . . .: 10.33.3.217
   Masque de sous-réseau. . . . . . . . . : 255.255.255.0
   Passerelle par défaut. . . . . . . . . : 10.33.3.253
```

```
 ping 8.8.8.8

Envoi d’une requête 'Ping'  8.8.8.8 avec 32 octets de données :
Réponse de 8.8.8.8 : octets=32 temps=19 ms TTL=115
Réponse de 8.8.8.8 : octets=32 temps=21 ms TTL=115
Réponse de 8.8.8.8 : octets=32 temps=21 ms TTL=115
Réponse de 8.8.8.8 : octets=32 temps=17 ms TTL=115

Statistiques Ping pour 8.8.8.8:
    Paquets : envoyés = 4, reçus = 4, perdus = 0 (perte 0%),
Durée approximative des boucles en millisecondes :
    Minimum = 17ms, Maximum = 21ms, Moyenne = 19ms
    
 ```
 
 ## II. Exploration locale en duo
 
3. Modification d'adresse IP
![](img/img5.png)
![](img/img4.png)
Ma machine a l'ip 192.168.1.2 et celle de mon équipier est 192.168.1.1
```
PS C:\Users\paula> ipconfig


Carte Ethernet Ethernet :

   Suffixe DNS propre à la connexion. . . :
   Description. . . . . . . . . . . . . . : Realtek PCIe GbE Family Controller
   Adresse physique . . . . . . . . . . . : 7C-8A-E1-43-30-B5
   DHCP activé. . . . . . . . . . . . . . : Non
   Configuration automatique activée. . . : Oui
   Adresse IPv6 de liaison locale. . . . .: fe80::d60:e76c:19a6:3cc5%5(préféré)
   Adresse IPv4. . . . . . . . . . . . . .: 192.168.1.2(préféré)
   Masque de sous-réseau. . . . . . . . . : 255.255.255.252
   Passerelle par défaut. . . . . . . . . :
   IAID DHCPv6 . . . . . . . . . . . : 108825313
   DUID de client DHCPv6. . . . . . . . : 00-01-00-01-26-96-1F-6D-7C-8A-E1-43-30-B5
   Serveurs DNS. . .  . . . . . . . . . . : 8.8.8.8
                                       1.1.1.1
   NetBIOS sur Tcpip. . . . . . . . . . . : Activé
   ```
   


``` 
PS C:\Users\paula> ping 192.168.1.1

 ping 192.168.1.1

Envoi d’une requête 'Ping'  192.168.1.1 avec 32 octets de données :
Réponse de 192.168.1.1 : octets=32 temps=2 ms TTL=128
Réponse de 192.168.1.1 : octets=32 temps=2 ms TTL=128
Réponse de 192.168.1.1 : octets=32 temps=2 ms TTL=128
Réponse de 192.168.1.1 : octets=32 temps=2 ms TTL=128

Statistiques Ping pour 192.168.1.1:
    Paquets : envoyés = 4, reçus = 4, perdus = 0 (perte 0%),
Durée approximative des boucles en millisecondes :
    Minimum = 2ms, Maximum = 2ms, Moyenne = 2ms
```
Les machines peuvent bien communiquer ensemble.

```
PS C:\Users\paula> arp -a
[...]
Interface : 192.168.1.2 --- 0x5
  Adresse Internet      Adresse physique      Type
  192.168.1.1           24-4b-fe-65-96-30     dynamique
  ```

4. Utilisation d'un des deux comme gateway

Nous avons mis en commum cette partie et lucas a fait la dernière partie, soit le firewall donc nos deux rendues vont se ressembler.

" Pour cette étape ma machine sera le gateway (192.168.1.1)
```
PS C:\WINDOWS\system32> ping 192.168.1.1

Envoi d’une requête 'Ping'  192.168.1.1 avec 32 octets de données :
Réponse de 192.168.1.1 : octets=32 temps=2 ms TTL=128
Réponse de 192.168.1.1 : octets=32 temps=2 ms TTL=128
Réponse de 192.168.1.1 : octets=32 temps=2 ms TTL=128
Réponse de 192.168.1.1 : octets=32 temps=2 ms TTL=128

Statistiques Ping pour 192.168.1.1:
    Paquets : envoyés = 4, reçus = 4, perdus = 0 (perte 0%),
Durée approximative des boucles en millisecondes :
    Minimum = 2ms, Maximum = 2ms, Moyenne = 2ms
    ```
l'autre machine peut bien communiquer avec la mienne
```
PS C:\WINDOWS\system32> ipconfig

Configuration IP de Windows


Carte Ethernet Ethernet :

   Suffixe DNS propre à la connexion. . . :
   Adresse IPv6 de liaison locale. . . . .: fe80::5dc0:10f3:5fb4:acb2%7
   Adresse IPv4. . . . . . . . . . . . . .: 192.168.1.2
   Masque de sous-réseau. . . . . . . . . : 255.255.255.252
   Passerelle par défaut. . . . . . . . . : 192.168.1.1
   ```
sa passerelle est bien ma machine
Pour partager la connexion nous avons suivi les étapes suivantes:
Panneau de configuration -> Réseau et internet -> Centre réseau et partage -> modifier les paramètres de la carte -> on sélectionne la bonne carte (WiFi dans notre cas) -> Propriétés -> Partage
![](img/img6.png)

```
PS C:\WINDOWS\system32> ping 8.8.8.8

Envoi d’une requête 'Ping'  8.8.8.8 avec 32 octets de données :
Réponse de 8.8.8.8 : octets=32 temps=35 ms TTL=114
Réponse de 8.8.8.8 : octets=32 temps=30 ms TTL=114
Réponse de 8.8.8.8 : octets=32 temps=73 ms TTL=114
Réponse de 8.8.8.8 : octets=32 temps=19 ms TTL=114

Statistiques Ping pour 8.8.8.8:
    Paquets : envoyés = 4, reçus = 4, perdus = 0 (perte 0%),
Durée approximative des boucles en millisecondes :
    Minimum = 19ms, Maximum = 73ms, Moyenne = 39ms
    ```

``` 
PS C:\WINDOWS\system32> tracert 8.8.8.8

Détermination de l’itinéraire vers dns.google [8.8.8.8]
avec un maximum de 30 sauts :

  1    <1 ms     *        2 ms  LAPTOP-4DFELSLI [192.168.1.1]
  [...]
  ```
sa machine passe bien par la mienne pour communiquer avec 8.8.8.8

5. Petit chat privé
pour cette partie ma machine sera la serveur
```
PS C:\Users\hurlu\Desktop\netcat-win32-1.11\netcat-1.11> ./nc64.exe -l -p 8888
dnbgkjdngkjndjkhnjth
allo
ici la voix
non
pas ça
```
du côté client ça donne ça:
```
PS C:\Users\Paula\Downloads\netcat-win32-1.11\netcat-1.11> ./nc64.exe 192.168.1.1 8888
dnbgkjdngkjndjkhnjth
allo
ici la voix
non
pas ça
```

Pour aller plus loin:
ici je suis encore le serveur
avec la premiere commande j'accepte uniquement les connexions venant de 192.168.1.1 (je suis 192.168.1.1 donc ça ne devrait pas marcher)
avec la deuxieme j'accepte uniquement celles venant de 192.168.1.2 étant donné que c'est l'adresse de la deuxieme machine ça fonctionne.
```
PS C:\Users\hurlu\Desktop\netcat-win32-1.11\netcat-1.11> ./nc64.exe -l -p 8888 192.168.1.1
invalid connection to [192.168.1.1] from (UNKNOWN) [192.168.1.2] 32525
```
```
PS C:\Users\hurlu\Desktop\netcat-win32-1.11\netcat-1.11> ./nc64.exe -l -p 8888 192.168.1.2
ok
test
coucou
```
du côté client ça donne ça :
```
PS C:\Users\Paula\Downloads\netcat-win32-1.11\netcat-1.11> ./nc64.exe 192.168.1.1 8888
ok
test
coucou
```
6. Firewall
nous avons réactivé le firewall, les ping ne passent donc plus,
```
PS C:\Users\hurlu> ping 192.168.1.2

Envoi d’une requête 'Ping'  192.168.1.2 avec 32 octets de données :
Délai d’attente de la demande dépassé.
Délai d’attente de la demande dépassé.
Délai d’attente de la demande dépassé.
Délai d’attente de la demande dépassé.

Statistiques Ping pour 192.168.1.2:
    Paquets : envoyés = 4, reçus = 0, perdus = 4 (perte 100%)
    ```

pour accepter les pings nous avons fait la manipulation suivante sur les deux machines:
pare-feu windows defender -> Paramètres avancés -> Règles du trafic entrant -> ajouter une nouvelle règle -> Personalisée -> Tous les programmes -> type de protocole ICMP v4 -> suivant jusqu'au nom, on met un nom puis terminer
```
PS C:\Users\hurlu> ping 192.168.1.2

Envoi d’une requête 'Ping'  192.168.1.2 avec 32 octets de données :
Réponse de 192.168.1.2 : octets=32 temps<1ms TTL=128
Réponse de 192.168.1.2 : octets=32 temps<1ms TTL=128
Réponse de 192.168.1.2 : octets=32 temps<1ms TTL=128
Réponse de 192.168.1.2 : octets=32 temps<1ms TTL=128

Statistiques Ping pour 192.168.1.2:
    Paquets : envoyés = 4, reçus = 4, perdus = 0 (perte 0%),
Durée approximative des boucles en millisecondes :
    Minimum = 0ms, Maximum = 0ms, Moyenne = 0ms
    ```
les pings fonctionnent désormais.
Pour utiliser netcat avec le firewall activé nous allons utiliser le port 1052
sur la machine qui servira de serveur nous allons créer 2 nouvelles règles de trafic entrant
même démarche que pour accepter les pings sauf que cette fois on en fait 2, une pour le protocole TCP sur le port 1052 et l'autre pour le protocole UDP sur le port 1052
du côté serveur ça donne ça :
```
PS C:\Users\hurlu\Desktop\netcat-win32-1.11\netcat-1.11> .\nc.exe -l -p 1052
test
bonjour
```
```
PS C:\Users\hurlu\Desktop\netcat-win32-1.11\netcat-1.11> .\nc.exe -l -p 1054
PS C:\Users\hurlu\Desktop\netcat-win32-1.11\netcat-1.11>
```
Pour la deuxieme commande cela ne fonctionnait pas car le port 1054 n'est pas ouvert mais seulement le port 1052 "



## III. Manipulations d'autres outils/protocoles côté client
1.DHCP
```
PS C:\Users\Paula> ipconfig /all

[...]
Bail obtenu. . . . . . . . . . . . . . : dimanche 19 septembre 2021 18:58:43
Bail expirant. . . . . . . . . . . . . : lundi 20 septembre 2021 19:11:31
Serveur DHCP . . . . . . . . . . . . . : 192.168.1.1
[...]
```
2.DNS

```
PS C:\Users\Paula> ipconfig /all


Serveurs DNS. . .  . . . . . . . . . . : fe80::aa9a:93ff:fe33:ad32%5
                                               192.168.1.1
                                    
PS C:\Users\Paula> nslookup google.com
Serveur :   UnKnown
Address:  fe80::aa9a:93ff:fe33:ad32

Réponse ne faisant pas autorité :
Nom :    google.com
Addresses:  2a00:1450:4007:80e::200e
          216.58.214.174

PS C:\Users\Paula> nslookup ynov.com
Serveur :   UnKnown
Address:  fe80::aa9a:93ff:fe33:ad32

Réponse ne faisant pas autorité :
Nom :    ynov.com
Address:  92.243.16.143
```

l'ip 216.58.214.174 est celle du nom de domaine google.com et ramène donc vers celui-ci.

l'ip 92.243.16.143 est celle du nom de domaine ynov.com et ramène donc vers celui-ci.

l'adresse est  fe80::aa9a:93ff:fe33:ad32 .

reverse lookup : 
```
PS C:\Users\Paula> nslookup 78.74.21.21
Serveur :   UnKnown
Address:  fe80::aa9a:93ff:fe33:ad32

Nom :    host-78-74-21-21.homerun.telia.com
Address:  78.74.21.21

PS C:\Users\Paula> nslookup 92.146.54.88
Serveur :   UnKnown
Address:  fe80::aa9a:93ff:fe33:ad32

Nom :    apoitiers-654-1-167-88.w92-146.abo.wanadoo.fr
Address:  92.146.54.88
```

On remarque que c'est la meme chose qu'avant , c'est à dire que la on obtient le nom de domaine et l'ip , en fait on peut trouver dans les deux sens soit avec l'ip soit avec le nom de domaine.
## IV. Wireshark

Etant donné que je n'avais pas 2 pc chez moi  à dispositions lucas a pu le faire avec ses deux pc , il m'a donc fait un partage d'écran. 
merci à lui =)

"
Pour cette partie je ne suis plus sur le réseau d'ynov,l'adresse de ma passerelle est donc 192.168.1.1, l'adresse du réseau entre les deux cartes ethernet a été modifié en 192.168.137.0/30 pour ne pas créer de confusions
```
PS C:\Users\hurlu> ping 192.168.1.1

Envoi d’une requête 'Ping'  192.168.1.1 avec 32 octets de données :
Réponse de 192.168.1.1 : octets=32 temps=3 ms TTL=64
Réponse de 192.168.1.1 : octets=32 temps=3 ms TTL=64
Réponse de 192.168.1.1 : octets=32 temps=3 ms TTL=64
Réponse de 192.168.1.1 : octets=32 temps=4 ms TTL=64

Statistiques Ping pour 192.168.1.1:
    Paquets : envoyés = 4, reçus = 4, perdus = 0 (perte 0%),
Durée approximative des boucles en millisecondes :
    Minimum = 3ms, Maximum = 4ms, Moyenne = 3ms
    ```

![](img/img7.png)
voici les trames du ping, on peut y voir chaque étape du ping, une série de 4 demandes/réponses
```
PS C:\Users\hurlu\Desktop\netcat-win32-1.11\netcat-1.11> .\nc.exe -l -p 1052
bonjour
salut
???
PS C:\Users\hurlu\Desktop\netcat-win32-1.11\netcat-1.11>
```

![](img/img8.png)
voici les trames du netcat, on peut y discerner un bout de message "bonjour"
```
PS C:\Users\hurlu\Desktop\netcat-win32-1.11\netcat-1.11> nslookup google.com
Serveur :   dns.google
Address:  8.8.8.8

Réponse ne faisant pas autorité :
Nom :    google.com
Addresses:  2a00:1450:4007:808::200e
          142.250.74.238
          ```

![](img/img9.png)
voici les trames de la requête dns, à droite on voit beaucoup de 8.8.8.8, c'est l'adresse du serveur dns à qui l'on pose une question 
"
