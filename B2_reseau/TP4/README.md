# TP4 : Vers un réseau d'entreprise

## SOMMAIRE : 

- [I. Dumb switch](#i-dumb-switch)
  - [1. Topologie 1](#1-topologie-1)
  - [2. Adressage topologie 1](#2-adressage-topologie-1)
  - [3. Setup topologie 1](#3-setup-topologie-1)
- [II. VLAN](#ii-vlan)
  - [1. Topologie 2](#1-topologie-2)
  - [2. Adressage topologie 2](#2-adressage-topologie-2)
    - [3. Setup topologie 2](#3-setup-topologie-2)
- [III. Routing](#iii-routing)
  - [1. Topologie 3](#1-topologie-3)
  - [2. Adressage topologie 3](#2-adressage-topologie-3)
  - [3. Setup topologie 3](#3-setup-topologie-3)
- [IV. NAT](#iv-nat)
  - [1. Topologie 4](#1-topologie-4)
  - [2. Adressage topologie 4](#2-adressage-topologie-4)
  - [3. Setup topologie 4](#3-setup-topologie-4)
- [V. Add a building](#v-add-a-building)
  - [1. Topologie 5](#1-topologie-5)
  - [2. Adressage topologie 4](#2-adressage-topologie-5)
  - [3. Setup topologie 5](#3-setup-topologie-5)


# I. Dumb switch

## 3. Setup topologie 1
🌞 Commençons simple
````
PC1>  ip 10.1.1.1/24
Checking for duplicate address...
PC1 : 10.1.1.1 255.255.255.0

PC1> show ip

NAME        : PC1[1]
IP/MASK     : 10.1.1.1/24
GATEWAY     : 0.0.0.0
DNS         :
MAC         : 00:50:79:66:68:00
LPORT       : 20008
RHOST:PORT  : 127.0.0.1:20009
MTU         : 1500
````
````
PC2> show ip

NAME        : PC2[1]
IP/MASK     : 10.1.1.2/24
GATEWAY     : 0.0.0.0
DNS         :
MAC         : 00:50:79:66:68:01
LPORT       : 20042
RHOST:PORT  : 127.0.0.1:20043
MTU         : 1500
````

# II. VLAN
## 3. Setup topologie 2
🌞 Adressage

PC1 et PC2 ont déjà été config.
````
PC3> ip 10.1.1.3/24
Checking for duplicate address...
PC3 : 10.1.1.3 255.255.255.0

PC3> show ip

NAME        : PC3[1]
IP/MASK     : 10.1.1.3/24
GATEWAY     : 0.0.0.0
DNS         :
MAC         : 00:50:79:66:68:02
LPORT       : 20046
RHOST:PORT  : 127.0.0.1:20047
MTU         : 1500
````
````
PC2> ping 10.1.1.3 -c 2

84 bytes from 10.1.1.3 icmp_seq=1 ttl=64 time=5.043 ms
84 bytes from 10.1.1.3 icmp_seq=2 ttl=64 time=4.191 ms

PC3> ping 10.1.1.1 -c 2

84 bytes from 10.1.1.1 icmp_seq=1 ttl=64 time=4.373 ms
84 bytes from 10.1.1.1 icmp_seq=2 ttl=64 time=3.146 ms

PC1> ping 10.1.1.2/24 -c 2

84 bytes from 10.1.1.2 icmp_seq=1 ttl=64 time=1.757 ms
84 bytes from 10.1.1.2 icmp_seq=2 ttl=64 time=1.516 ms

````
🌞 Configuration des VLANs
````
Switch(config)#do show ip int br
Interface              IP-Address      OK? Method Status                Protocol
GigabitEthernet0/0     unassigned      YES unset  up                    up
GigabitEthernet0/1     unassigned      YES unset  up                    up
GigabitEthernet0/2     unassigned      YES unset  up                    up
GigabitEthernet0/3     unassigned      YES unset  up                    up
GigabitEthernet1/0     unassigned      YES unset  down                  down
GigabitEthernet1/1     unassigned      YES unset  down                  down
GigabitEthernet1/2     unassigned      YES unset  down                  down
GigabitEthernet1/3     unassigned      YES unset  down                  down
GigabitEthernet2/0     unassigned      YES unset  down                  down
GigabitEthernet2/1     unassigned      YES unset  down                  down
GigabitEthernet2/2     unassigned      YES unset  down                  down
GigabitEthernet2/3     unassigned      YES unset  down                  down
GigabitEthernet3/0     unassigned      YES unset  down                  down
GigabitEthernet3/1     unassigned      YES unset  down                  down
GigabitEthernet3/2     unassigned      YES unset  down                  down
GigabitEthernet3/3     unassigned      YES unset  down                  down
Switch(config)#interface GigabitEthernet0/1
Switch(config-if)#switchport mode access
Switch(config-if)#switchport access vlan 10
Switch(config-if)#exit
Switch(config)#interface GigabitEthernet0/2
Switch(config-if)#switchport mode access
Switch(config-if)#switchport access vlan 10
Switch(config-if)#exit
Switch(config)#interface GigabitEthernet0/3
Switch(config-if)#switchport mode access
Switch(config-if)#switchport access vlan 20
Switch(config-if)#exit

Switch#show vlan

VLAN Name                             Status    Ports
---- -------------------------------- --------- -------------------------------
1    default                          active    Gi0/0, Gi1/0, Gi1/1, Gi1/2
                                                Gi1/3, Gi2/0, Gi2/1, Gi2/2
                                                Gi2/3, Gi3/0, Gi3/1, Gi3/2
                                                Gi3/3
10   PC1 PC2                          active    Gi0/1, Gi0/2
20   PC3                              active    Gi0/3

````
Tout est bon, vlan 10 se nomme PC1 PC2 et vlan 20 se nomme PC3.

🌞 Vérif
Ping PC1 vers PC2 : 

````
PC1> ping 10.1.1.2/24 -c 2

84 bytes from 10.1.1.2 icmp_seq=1 ttl=64 time=2.331 ms
84 bytes from 10.1.1.2 icmp_seq=2 ttl=64 time=1.528 ms
````

ping PC3 vers PC1 et PC2 : 

````
PC3> ping 10.1.1.1

host (10.1.1.1) not reachable

PC3> ping 10.1.1.2

host (10.1.1.2) not reachable

````
C'est OKKKAYY!


# III. Routing
## 3. Setup topologie 3

🌞 Adressage

setup vm : 

```
[paul@web1 ~]$ sudo cat /etc/sysconfig/network-scripts/ifcfg-enp0s3
[sudo] password for paul:

BOOTPROTO=static
NAME=enp0s3
DEVICE=enp0s3
ONBOOT=yes
IPADDR=10.3.3.1
NETMASK=255.255.255.0
```

setup sur gns :

````
PC1> show ip

NAME        : PC1[1]
IP/MASK     : 10.1.1.1/24
GATEWAY     : 0.0.0.0
DNS         :
MAC         : 00:50:79:66:68:00
LPORT       : 20010
RHOST:PORT  : 127.0.0.1:20011
MTU         : 1500

PC1> wr
Saving startup configuration to startup.vpc
.  done
````
````
PC2> show ip

NAME        : PC2[1]
IP/MASK     : 10.1.1.2/24

````
````
adm1> show ip

NAME        : adm1[1]
IP/MASK     : 10.2.2.1/24

````

🌞 Configuration des VLANs
* déclaration des VLANs sur le switch
````
Switch>en
Switch#conf t
Enter configuration commands, one per line.  End with CNTL/Z.
Switch(config)#vlan 11
Switch(config-vlan)#name clients
Switch(config-vlan)#exit
Switch(config)#vlan 12
Switch(config-vlan)#name admins
Switch(config-vlan)#exit
Switch(config)#vlan 13
Switch(config-vlan)#name servers
Switch(config-vlan)#exit
Switch(config)#
````
* ajout des ports du switches dans le bon VLAN

PC1 ; PC2 ; adm1 ; web1
````
Switch#conf t
Enter configuration commands, one per line.  End with CNTL/Z.
Switch(config)#interface GigabitEthernet0/1
Switch(config-if)#switchport mode access
Switch(config-if)#switchport access vlan 11
Switch(config-if)#exit
Switch(config)#interface GigabitEthernet0/2
Switch(config-if)#switchport mode access
Switch(config-if)#switchport access vlan 11
Switch(config-if)#exit
Switch(config)#interface GigabitEthernet0/3
Switch(config-if)#switchport mode access
Switch(config-if)#switchport access vlan 12
Switch(config-if)#exit
Switch(config)#interface GigabitEthernet1/0
Switch(config-if)#switchport mode access
Switch(config-if)#switchport access vlan 13
Switch(config-if)#exit
Switch(config)#exit
Switch#

````
résultat : 
````
Switch(config)#do show vlan br

VLAN Name                             Status    Ports
---- -------------------------------- --------- -------------------------------
1    default                          active    Gi0/0, Gi1/1, Gi1/2, Gi1/3
                                                Gi2/0, Gi2/1, Gi2/2, Gi2/3
                                                Gi3/0, Gi3/1, Gi3/2, Gi3/3
11   clients                          active    Gi0/1, Gi0/2
12   admins                           active    Gi0/3
13   servers                          active    Gi1/0

````
* il faudra ajouter le port qui pointe vers le routeur comme un trunk : c'est un port entre deux équipements réseau (un switch et un routeur)

````
Switch(config)#interface GigabitEthernet0/0
Switch(config-if)#switchport trunk encapsulation dot1q
Switch(config-if)#switchport mode trunk
Switch(config-if)#switchport trunk allowed vlan add 11,12,13
Switch(config-if)#exit
Switch(config)#exit
Switch#sh
*Oct 24 23:41:13.868: %SYS-5-CONFIG_I: Configured from console by consoleow interface trunk

Port        Mode             Encapsulation  Status        Native vlan
Gi0/0       on               802.1q         trunking      1

Port        Vlans allowed on trunk
Gi0/0       1-4094

Port        Vlans allowed and active in management domain
Gi0/0       1,11-13

Port        Vlans in spanning tree forwarding state and not pruned
Gi0/0       1,11-13
Switch#wr
Building configuration...
Compressed configuration from 3767 bytes to 1692 bytes[OK]
Switch#
*Oct 24 23:41:49.975: %GRUB-5-CONFIG_WRITING: GRUB configuration is being updated on disk. Please wait...
*Oct 24 23:41:50.682: %GRUB-5-CONFIG_WRITTEN: GRUB configuration was written to disk successfully.
Switch#

````

➜ Pour le routeur
🌞 Config du routeur

````
R1(config)#interface fastEthernet 0/0.11
R1(config-subif)#encapsulation dot1Q 11
R1(config-subif)#ip addr 10.1.1.254 255.255.255.0
R1(config-subif)#exit
R1(config)#interface fastEthernet 0/0.12
R1(config-subif)#encapsulation dot1Q 12
R1(config-subif)#ip addr 10.2.2.254 255.255.255.0
R1(config-subif)#exit
R1(config)#interface fastEthernet 0/0.13
R1(config-subif)#encapsulation dot1Q 13
R1(config-subif)#ip addr 10.3.3.254 255.255.255.0
R1(config-subif)#exit
R1(config)#exit
R1#
*Mar  1 05:09:18.738: %SYS-5-CONFIG_I: Configured from console by console
R1#conf t
Enter configuration commands, one per line.  End with CNTL/Z.
R1(config)#interface fastEthernet 0/0
R1(config-if)#no shut
R1(config-if)#e
*Mar  1 05:10:00.438: %LINK-3-UPDOWN: Interface FastEthernet0/0, changed state to up
*Mar  1 05:10:01.438: %LINEPROTO-5-UPDOWN: Line protocol on Interface FastEthernet0/0, changed state to up
R1(config-if)#exit
R1(config)#exit
R1#wr
*Mar  1 05:10:14.914: %SYS-5-CONFIG_I: Configured from console by console
R1#wr
Building configuration...
[OK]

````

🌞 Vérif

* Tout le monde doit pouvoir ping le routeur sur l'IP qui est dans son réseau
````
PC1> ping 10.1.1.254 -c 2

84 bytes from 10.1.1.254 icmp_seq=1 ttl=255 time=7.812 ms
84 bytes from 10.1.1.254 icmp_seq=2 ttl=255 time=6.132 ms
````
````
PC2> ping 10.1.1.254

84 bytes from 10.1.1.254 icmp_seq=1 ttl=255 time=9.863 ms
84 bytes from 10.1.1.254 icmp_seq=2 ttl=255 time=11.455 ms
````
````
adm1> ping 10.2.2.254

84 bytes from 10.2.2.254 icmp_seq=1 ttl=255 time=9.457 ms
84 bytes from 10.2.2.254 icmp_seq=2 ttl=255 time=8.310 ms
````
````
[paul@web1 ~]$ ping 10.3.3.254
PING 10.3.3.254 (10.3.3.254) 56(84) bytes of data.
64 bytes from 10.3.3.254: icmp_seq=l tt1=255 time=22.0 ms 64 bytes from 10.3.3.254: icmp_seq=2 tt1=255 time=9.23 ms
"C
--- 10.3.3.254 ping statistics ---	
2 packets transmitted, 2 received, 0% packet loss, time 1002ms rtt min/avg/max/mdev = 9.226/15.634/22.042/6.408 ms 
````


* En ajoutant une route vers les réseaux, ils peuvent se ping entre eux

ajoutez une route par défaut sur les VPCS

````
PC1> ip 10.1.1.1/24 10.1.1.254
Checking for duplicate address...
PC1 : 10.1.1.1 255.255.255.0 gateway 10.1.1.254
````
````
PC2> ip 10.1.1.2/24 10.1.1.254
Checking for duplicate address...
PC2 : 10.1.1.2 255.255.255.0 gateway 10.1.1.254
````
````
adm1> ip 10.2.2.1/24 10.2.2.254
Checking for duplicate address...
adm1 : 10.2.2.1 255.255.255.0 gateway 10.2.2.254
````


ajoutez une route par défaut sur la machine virtuelle
````
[paul@web1 ~]$ sudo cat /etc/sysconfig/network
GATEWAY=10.3.3.254
````
testez des ping entre les réseaux

````
PC1> ping 10.2.2.1 -c 2

84 bytes from 10.2.2.1 icmp_seq=1 ttl=63 time=23.779 ms
84 bytes from 10.2.2.1 icmp_seq=2 ttl=63 time=15.395 ms
````
````
adm1> ping 10.3.3.1 -c 2

84 bytes from 10.3.3.1 icmp_seq=1 ttl=63 time=29.354 ms
84 bytes from 10.3.3.1 icmp_seq=2 ttl=63 time=14.030 ms
````
````
[paul@web1 ~]$ ping 10.1.1.2 -c 2
PING 10.1.1.2 (10.1.1.2) 56(84) bytes of data.
64 bytes from 10.1.1.2: icmp_seq=l ttl=63 time=18.3 ms 64 bytes from 10.1.1.2: icmp_seq=2 ttl=63 time=23.3 ms
--- 10.1.1.2 ping statistics --- 	
2 packets transmitted, 2 received, 0% packet loss, time 1002ms
rtt min/avg/max/mdev = 18.291/20.785/23.279/2.484 ms

````
# IV. NAT

## 3. Setup topologie 4
🌞 Ajoutez le noeud Cloud à la topo
````
R1#conf t
Enter configuration commands, one per line.  End with CNTL/Z.
R1(config)#interface FastEthernet1/0
R1(config-if)#
R1(config-if)#ip address dhcp
R1(config-if)#no shut
R1(config-if)#
*Mar  1 00:17:47.559: %LINK-3-UPDOWN: Interface FastEthernet1/0, changed state to up
*Mar  1 00:17:48.559: %LINEPROTO-5-UPDOWN: Line protocol on Interface FastEthernet1/0, changed state to up
R1(config-if)#
*Mar  1 00:17:55.595: %DHCP-6-ADDRESS_ASSIGN: Interface FastEthernet1/0 assigned DHCP address 10.0.3.16, mask 255.255.255.0, hostname R1

R1(config-if)#exit
R1(config)#show ip int br
             ^
% Invalid input detected at '^' marker.

R1(config)#exit
R1#show ip int br
Interface                  IP-Address      OK? Method Status                Protocol
[...]
FastEthernet1/0            10.0.3.16       YES DHCP   up                    up
[...]

````
````
PC1> ping 1.1.1.1 -c 2

84 bytes from 1.1.1.1 icmp_seq=1 ttl=62 time=17.247 ms
84 bytes from 1.1.1.1 icmp_seq=2 ttl=62 time=14.714 ms
````
````
R1#ping 1.1.1.1

Type escape sequence to abort.
Sending 5, 100-byte ICMP Echos to 1.1.1.1, timeout is 2 seconds:
!!!!!
Success rate is 100 percent (5/5), round-trip min/avg/max = 4/13/32 ms

````

🌞 Configurez le NAT

````
R1(config)#interface FastEthernet1/0
R1(config-if)#ip nat outside

*Mar  1 00:58:01.395: %LINEPROTO-5-UPDOWN: Line protocol on Interface NVI0, changed state to up
R1(config-if)#exit
R1(config)#interface FastEthernet0/0
R1(config-if)#ip nat inside
R1(config-if)#exit
R1(config)#access-list 1 permit any
R1(config)#ip nat inside source list 1 interface fastEthernet 1/0 overload

````

🌞 Test
````
PC1> show ip

NAME        : PC1[1]
IP/MASK     : 10.1.1.1/24
GATEWAY     : 10.1.1.254
DNS         : 1.1.1.1
MAC         : 00:50:79:66:68:00
LPORT       : 20015
RHOST:PORT  : 127.0.0.1:20016
MTU         : 1500

````
````
PC2> show ip

NAME        : PC2[1]
IP/MASK     : 10.1.1.2/24
GATEWAY     : 10.1.1.254
DNS         : 1.1.1.1
MAC         : 00:50:79:66:68:01
LPORT       : 20049
RHOST:PORT  : 127.0.0.1:20050
MTU         : 1500

````
````
adm1> show ip

NAME        : adm1[1]
IP/MASK     : 10.2.2.1/24
GATEWAY     : 10.2.2.254
DNS         : 1.1.1.1
MAC         : 00:50:79:66:68:02
LPORT       : 20051
RHOST:PORT  : 127.0.0.1:20052
MTU         : 1500

````
````
[paul@web1 ~]$ sudo cat /etc/sysconfig/network
GATEWAY=10.3.3.254

[paul@web1 ~]$ sudo cat /etc/resolv.conf
nameserver 192.168.1.254
nameserver 1.1.1.1

[paul@web1 ~]$ sudo cat /etc/sysconfig/network-scripts/ifcfg-enp0s3
BOOTPROTO=static 
NAME=enp0s3 
DEVICE=enp0s3 
ONBOOT=yes 
IPADDR=10.3.3.1 
NETMASK=255.255.255.0 
GATEWAY=10.3.3.254 
dns1=1.1.1.1

````

PING : 

````

PC1> ping google.com
google.com resolved to 216.58.209.238

84 bytes from 216.58.209.238 icmp_seq=1 ttl=114 time=31.328 ms
84 bytes from 216.58.209.238 icmp_seq=2 ttl=114 time=25.562 ms
````
````
adm1> ping youtube.com
youtube.com resolved to 172.217.22.142

84 bytes from 172.217.22.142 icmp_seq=1 ttl=113 time=31.121 ms
84 bytes from 172.217.22.142 icmp_seq=2 ttl=113 time=33.700 ms
````
````
[paul@web1 ~]$ ping google.com -c 2
PING google.com (142.250.179.110) 56(84) bytes of data.
64 bytes from par21s20-in-f14.1e100.net (142.250.179.110): icmp_seq=1 ttl=113 time=64.1 ms
64 bytes from par21s20-in-f14.1e100.net (142.250.179.110: icmp_seq=2 ttl=113 time=87.5 ms

````

# V. Add a building
## 3. Setup topologie 5
🌞  Vous devez me rendre le show running-config de tous les équipements

````

[paul@dhcp ~]$ [paul@dhcp ~]$ sudo cat /etc/dhcp/dhcpd.conf
[sudo] password for paul:

default-lease-time 800;
max-lease-time 11800;
ddns-update-style none;
authoritative;
subnet 10.1.1.0 netmask 255.255.255.0 {
        range                           10.1.1.20 10.1.1.100;
        option domain-name-servers      8.8.8.8;
        option routers                  10.1.1.254;
}

}
````

ROUTEUR 1 : 

````
R1#show run
Building configuration...

Current configuration : 1336 bytes
!
version 12.4
service timestamps debug datetime msec
service timestamps log datetime msec
no service password-encryption
!
hostname R1
!
boot-start-marker
boot-end-marker
!
!
no aaa new-model
memory-size iomem 5
no ip icmp rate-limit unreachable
!
!
ip cef
!
!
!
!
!
!
!
!
!
!
!
!
!
!
!
!
ip tcp synwait-time 5
!
!
!
interface FastEthernet0/0
 no ip address
 ip nat inside
 ip virtual-reassembly
 duplex auto
 speed auto
!
interface FastEthernet0/0.11
 encapsulation dot1Q 11
 ip address 10.1.1.254 255.255.255.0
!
interface FastEthernet0/0.12
 encapsulation dot1Q 12
 ip address 10.2.2.254 255.255.255.0
!
interface FastEthernet0/0.13
 encapsulation dot1Q 13
 ip address 10.3.3.254 255.255.255.0
!
interface FastEthernet1/0
 ip address dhcp
 ip nat outside
 ip virtual-reassembly
 duplex auto
 speed auto
!
interface FastEthernet2/0
 no ip address
 shutdown
 duplex auto
 speed auto
!
interface FastEthernet3/0
 no ip address
 shutdown
 duplex auto
 speed auto
!
!
no ip http server
ip forward-protocol nd
!
!
ip nat inside source list 1 interface FastEthernet1/0 overload
!
access-list 1 permit any
no cdp log mismatch duplex
!
!
!
control-plane
!
!
!
!
!
!
!
!
!
line con 0
 exec-timeout 0 0
 privilege level 15
 logging synchronous
line aux 0
 exec-timeout 0 0
 privilege level 15
 logging synchronous
line vty 0 4
 login
!
!
end

````

SWITCH 1 : 

````
witch#show run
Building configuration...

Current configuration : 3685 bytes
!
! Last configuration change at 14:48:44 UTC Thu Oct 28 2021
!
version 15.2
service timestamps debug datetime msec
service timestamps log datetime msec
no service password-encryption
service compress-config
!
hostname Switch
!
boot-start-marker
boot-end-marker
!
!
!
no aaa new-model
!
!
!
!
!
!
!
!
ip cef
no ipv6 cef
!
!
!
spanning-tree mode pvst
spanning-tree extend system-id
!
vlan internal allocation policy ascending
!
!
!
!
!
!
!
!
!
!
!
!
!
!
interface GigabitEthernet0/0
 switchport trunk encapsulation dot1q
 switchport mode trunk
 media-type rj45
 negotiation auto
!
interface GigabitEthernet0/1
 switchport trunk encapsulation dot1q
 switchport mode trunk
 media-type rj45
 negotiation auto
!
interface GigabitEthernet0/2
 switchport trunk encapsulation dot1q
 switchport mode trunk
 media-type rj45
 negotiation auto
!
interface GigabitEthernet0/3
 media-type rj45
 negotiation auto
!
interface GigabitEthernet1/0
 media-type rj45
 negotiation auto
!
interface GigabitEthernet1/1
 media-type rj45
 negotiation auto
!
interface GigabitEthernet1/2
 media-type rj45
 negotiation auto
!
interface GigabitEthernet1/3
 media-type rj45
 negotiation auto
!
interface GigabitEthernet2/0
 media-type rj45
 negotiation auto
!
interface GigabitEthernet2/1
 media-type rj45
 negotiation auto
!
interface GigabitEthernet2/2
 media-type rj45
 negotiation auto
!
interface GigabitEthernet2/3
 media-type rj45
 negotiation auto
!
interface GigabitEthernet3/0
 media-type rj45
 negotiation auto
!
interface GigabitEthernet3/1
 media-type rj45
 negotiation auto
!
interface GigabitEthernet3/2
 media-type rj45
 negotiation auto
!
interface GigabitEthernet3/3
 media-type rj45
 negotiation auto
!
ip forward-protocol nd
!
no ip http server
no ip http secure-server
!
!
!
!
!
!
control-plane
!
banner exec ^C
**************************************************************************
* IOSv is strictly limited to use for evaluation, demonstration and IOS  *
* education. IOSv is provided as-is and is not supported by Cisco's      *
* Technical Advisory Center. Any use or disclosure, in whole or in part, *
* of the IOSv Software or Documentation to any third party for any       *
* purposes is expressly prohibited except as otherwise authorized by     *
* Cisco in writing.                                                      *
**************************************************************************^C
banner incoming ^C
**************************************************************************
* IOSv is strictly limited to use for evaluation, demonstration and IOS  *
* education. IOSv is provided as-is and is not supported by Cisco's      *
* Technical Advisory Center. Any use or disclosure, in whole or in part, *
* of the IOSv Software or Documentation to any third party for any       *
* purposes is expressly prohibited except as otherwise authorized by     *
* Cisco in writing.                                                      *
**************************************************************************^C
banner login ^C
**************************************************************************
* IOSv is strictly limited to use for evaluation, demonstration and IOS  *
* education. IOSv is provided as-is and is not supported by Cisco's      *
* Technical Advisory Center. Any use or disclosure, in whole or in part, *
* of the IOSv Software or Documentation to any third party for any       *
* purposes is expressly prohibited except as otherwise authorized by     *
* Cisco in writing.                                                      *
**************************************************************************^C
!
line con 0
line aux 0
line vty 0 4
!
!
end

````

SWITCH 2 : 

````
Switch#show run
Building configuration...

Current configuration : 3767 bytes
!
! Last configuration change at 14:42:00 UTC Thu Oct 28 2021
!
version 15.2
service timestamps debug datetime msec
service timestamps log datetime msec
no service password-encryption
service compress-config
!
hostname Switch
!
boot-start-marker
boot-end-marker
!
!
!
no aaa new-model
!
!
!
!
!
!
!
!
ip cef
no ipv6 cef
!
!
!
spanning-tree mode pvst
spanning-tree extend system-id
!
vlan internal allocation policy ascending
!
!
!
!
!
!
!
!
!
!
!
!
!
!
interface GigabitEthernet0/0
 switchport trunk encapsulation dot1q
 switchport mode trunk
 media-type rj45
 negotiation auto
!
interface GigabitEthernet0/1
 switchport access vlan 11
 switchport mode access
 media-type rj45
 negotiation auto
!
interface GigabitEthernet0/2
 switchport access vlan 11
 switchport mode access
 media-type rj45
 negotiation auto
!
interface GigabitEthernet0/3
 switchport access vlan 12
 switchport mode access
 media-type rj45
 negotiation auto
!
interface GigabitEthernet1/0
 switchport access vlan 13
 switchport mode access
 media-type rj45
 negotiation auto
!
interface GigabitEthernet1/1
 media-type rj45
 negotiation auto
!
interface GigabitEthernet1/2
 media-type rj45
 negotiation auto
!
interface GigabitEthernet1/3
 media-type rj45
 negotiation auto
!
interface GigabitEthernet2/0
 media-type rj45
 negotiation auto
!
interface GigabitEthernet2/1
 media-type rj45
 negotiation auto
!
interface GigabitEthernet2/2
 media-type rj45
 negotiation auto
!
interface GigabitEthernet2/3
 media-type rj45
 negotiation auto
!
interface GigabitEthernet3/0
 media-type rj45
 negotiation auto
!
interface GigabitEthernet3/1
 media-type rj45
 negotiation auto
!
interface GigabitEthernet3/2
 media-type rj45
 negotiation auto
!
interface GigabitEthernet3/3
 media-type rj45
 negotiation auto
!
ip forward-protocol nd
!
no ip http server
no ip http secure-server
!
!
!
!
!
!
control-plane
!
banner exec ^C
**************************************************************************
* IOSv is strictly limited to use for evaluation, demonstration and IOS  *
* education. IOSv is provided as-is and is not supported by Cisco's      *
* Technical Advisory Center. Any use or disclosure, in whole or in part, *
* of the IOSv Software or Documentation to any third party for any       *
* purposes is expressly prohibited except as otherwise authorized by     *
* Cisco in writing.                                                      *
**************************************************************************^C
banner incoming ^C
**************************************************************************
* IOSv is strictly limited to use for evaluation, demonstration and IOS  *
* education. IOSv is provided as-is and is not supported by Cisco's      *
* Technical Advisory Center. Any use or disclosure, in whole or in part, *
* of the IOSv Software or Documentation to any third party for any       *
* purposes is expressly prohibited except as otherwise authorized by     *
* Cisco in writing.                                                      *
**************************************************************************^C
banner login ^C
**************************************************************************
* IOSv is strictly limited to use for evaluation, demonstration and IOS  *
* education. IOSv is provided as-is and is not supported by Cisco's      *
* Technical Advisory Center. Any use or disclosure, in whole or in part, *
* of the IOSv Software or Documentation to any third party for any       *
* purposes is expressly prohibited except as otherwise authorized by     *
* Cisco in writing.                                                      *
**************************************************************************^C
!
line con 0
line aux 0
line vty 0 4
!
!
end

````

SWITCH 3 : 

````
Switch#show run
Building configuration...

Current configuration : 3767 bytes
!
! Last configuration change at 14:50:29 UTC Thu Oct 28 2021
!
version 15.2
service timestamps debug datetime msec
service timestamps log datetime msec
no service password-encryption
service compress-config
!
hostname Switch
!
boot-start-marker
boot-end-marker
!
!
!
no aaa new-model
!
!
!
!
!
!
!
!
ip cef
no ipv6 cef
!
!
!
spanning-tree mode pvst
spanning-tree extend system-id
!
vlan internal allocation policy ascending
!
!
!
!
!
!
!
!
!
!
!
!
!
!
interface GigabitEthernet0/0
 switchport trunk encapsulation dot1q
 switchport mode trunk
 media-type rj45
 negotiation auto
!
interface GigabitEthernet0/1
 switchport access vlan 11
 switchport mode access
 media-type rj45
 negotiation auto
!
interface GigabitEthernet0/2
 switchport access vlan 11
 switchport mode access
 media-type rj45
 negotiation auto
!
interface GigabitEthernet0/3
 switchport access vlan 11
 switchport mode access
 media-type rj45
 negotiation auto
!
interface GigabitEthernet1/0
 switchport access vlan 11
 switchport mode access
 media-type rj45
 negotiation auto
!
interface GigabitEthernet1/1
 media-type rj45
 negotiation auto
!
interface GigabitEthernet1/2
 media-type rj45
 negotiation auto
!
interface GigabitEthernet1/3
 media-type rj45
 negotiation auto
!
interface GigabitEthernet2/0
 media-type rj45
 negotiation auto
!
interface GigabitEthernet2/1
 media-type rj45
 negotiation auto
!
interface GigabitEthernet2/2
 media-type rj45
 negotiation auto
!
interface GigabitEthernet2/3
 media-type rj45
 negotiation auto
!
interface GigabitEthernet3/0
 media-type rj45
 negotiation auto
!
interface GigabitEthernet3/1
 media-type rj45
 negotiation auto
!
interface GigabitEthernet3/2
 media-type rj45
 negotiation auto
!
interface GigabitEthernet3/3
 media-type rj45
 negotiation auto
!
ip forward-protocol nd
!
no ip http server
no ip http secure-server
!
!
!
!
!
!
control-plane
!
banner exec ^C
**************************************************************************
* IOSv is strictly limited to use for evaluation, demonstration and IOS  *
* education. IOSv is provided as-is and is not supported by Cisco's      *
* Technical Advisory Center. Any use or disclosure, in whole or in part, *
* of the IOSv Software or Documentation to any third party for any       *
* purposes is expressly prohibited except as otherwise authorized by     *
* Cisco in writing.                                                      *
**************************************************************************^C
banner incoming ^C
**************************************************************************
* IOSv is strictly limited to use for evaluation, demonstration and IOS  *
* education. IOSv is provided as-is and is not supported by Cisco's      *
* Technical Advisory Center. Any use or disclosure, in whole or in part, *
* of the IOSv Software or Documentation to any third party for any       *
* purposes is expressly prohibited except as otherwise authorized by     *
* Cisco in writing.                                                      *
**************************************************************************^C
banner login ^C
**************************************************************************
* IOSv is strictly limited to use for evaluation, demonstration and IOS  *
* education. IOSv is provided as-is and is not supported by Cisco's      *
* Technical Advisory Center. Any use or disclosure, in whole or in part, *
* of the IOSv Software or Documentation to any third party for any       *
* purposes is expressly prohibited except as otherwise authorized by     *
* Cisco in writing.                                                      *
**************************************************************************^C
!
line con 0
line aux 0
line vty 0 4
!
!
end

````



🌞  Mettre en place un serveur DHCP dans le nouveau bâtiment
````
[paul@dhcp1 ~]$ sudo cat /etc/hostname
[sudo] password for paul:
dhcp1.client1.tp4
[paul@dhcp1 ~]$ sudo cat /etc/sysconfig/network-scripts/ifcfg-enp0s3
TYPE=Ethernet
BOOTPROTO=static
IPADDR=10.1.1.253
NETMASK=255.255.255.0
GATEWAY=10.1.1.254
DEFROUTE=yes
NAME=enp0s3
UUID=ac45d661-971a-49bb-86f8-779a49a77017
DEVICE=enp0s3
ONBOOT=yes
[paul@dhcp1 ~]$ sudo cat /etc/dhcp/dhcpd.conf
#
# DHCP Server Configuration file.
#   see /usr/share/doc/dhcp-server/dhcpd.conf.example
#   see dhcpd.conf(5) man page
#
# Bail de 24H, max 48H
default-lease-time 86400;
max-lease-time 172800;

subnet 10.1.1.0 netmask 255.255.255.0 {
        range                           10.1.1.20 10.1.1.100;
        option domain-name-servers      8.8.8.8;
        option routers                  10.1.1.254;
}
````





🌞  Vérification


recup de l'ip : 

````
PC3> show ip

NAME        : PC3[1]
IP/MASK     : 0.0.0.0/0
GATEWAY     : 0.0.0.0
DNS         :
MAC         : 00:50:79:66:68:03
LPORT       : 20128
RHOST:PORT  : 127.0.0.1:20129
MTU         : 1500

PC3> dhcp
DDORA IP 10.1.1.23/24 GW 10.1.1.254

PC3> show ip

NAME        : PC3[1]
IP/MASK     : 10.1.1.23/24
GATEWAY     : 10.1.1.254
DNS         : 8.8.8.8
DHCP SERVER : 10.1.1.253
DHCP LEASE  : 71893, 71898/35949/62910
MAC         : 00:50:79:66:68:03
LPORT       : 20128
RHOST:PORT  : 127.0.0.1:20129
MTU         : 1500

````

PING vers web1 : 

````
PC3> ping 10.3.3.1 -c 2

84 bytes from 10.3.3.1 icmp_seq=1 ttl=63 time=67.109 ms
84 bytes from 10.3.3.1 icmp_seq=2 ttl=63 time=57.540 ms


````

````
PC3> ping 8.8.8.8 -c 2

8.8.8.8 icmp_seq=1 timeout
84 bytes from 8.8.8.8 icmp_seq=2 ttl=112 time=65.745 ms

````
````
PC3> ping google.com -c 2
google.com resolved to 216.58.214.78

84 bytes from 216.58.214.78 icmp_seq=1 ttl=112 time=47.745 ms
84 bytes from 216.58.214.78 icmp_seq=2 ttl=112 time=67.456 ms


````

