# TP3 : Progressons vers le réseau d'infrastructure


# Sommaire

- [TP3 : Progressons vers le réseau d'infrastructure](#tp3--progressons-vers-le-réseau-dinfrastructure)
- [Sommaire](#sommaire)
- [I. (mini)Architecture réseau](#i-miniarchitecture-réseau)
  - [1. Adressage](#1-adressage)
  - [2. Routeur](#2-routeur)
- [II. Services d'infra](#ii-services-dinfra)
  - [1. Serveur DHCP](#1-serveur-dhcp)
  - [2. Serveur DNS](#2-serveur-dns)
    - [A. Our own DNS server](#a-our-own-dns-server)
    - [B. SETUP copain](#b-setup-copain)
  - [3. Get deeper](#3-get-deeper)
    - [A. DNS forwarder](#a-dns-forwarder)
    - [B. On revient sur la conf du DHCP](#b-on-revient-sur-la-conf-du-dhcp)
- [Entracte](#entracte)
- [III. Services métier](#iii-services-métier)
  - [1. Serveur Web](#1-serveur-web)
  - [2. Partage de fichiers](#2-partage-de-fichiers)
    - [A. L'introduction wola](#a-lintroduction-wola)
    - [B. Le setup wola](#b-le-setup-wola)
- [IV. Un peu de théorie : TCP et UDP](#iv-un-peu-de-théorie--tcp-et-udp)
- [V. El final](#v-el-final)

# I. (mini)Architecture réseau

## 1. Adressage

🌞 **Vous me rendrez un 🗃️ tableau des réseaux 🗃️ qui rend compte des adresses choisies, sous la forme** :

| Nom du réseau | Adresse du réseau | Masque        | Nombre de clients possibles | Adresse passerelle | [Adresse broadcast](../../cours/lexique/README.md#adresse-de-diffusion-ou-broadcast-address) |
|---------------|-------------------|---------------|-----------------------------|--------------------|------------------------------------|
| `client1`     | `	10.3.1.128`        | `255.255.255.192` | 62                | `10.3.1.190`         | `10.3.1.191`                  |
| `server1`     | `10.3.1.0`        | `255.255.255.128` |    126                        | 10.3.1.126         | `10.3.1.127`                      |
| `server2`     | `10.3.1.192`        | `255.255.255.240` | 14                          | `10.3.1.206`         | `10.3.1.207`                      |

🌞 **Vous remplirez aussi** au fur et à mesure que vous avancez dans le TP, au fur et à mesure que vous créez des machines, **le 🗃️ tableau d'adressage 🗃️ suivant :**

| Nom machine  | Adresse de la machine dans le réseau `client1` | Adresse dans `server1` | Adresse dans `server2` | Adresse de passerelle |
|--------------|------------------------------------------------|------------------------|------------------------|-----------------------|
| `router.tp3`         | `10.3.1.190/26`               | `10.3.1.126/25`      | `10.3.1.206/28`      | Carte NAT             |
| `dhcp.client1.tp3`   | `10.3.1.130/26`               | ...                  | ...                  | `10.3.1.190/26`       |
| `marcel.client1.tp3` | `10.3.1.131-10.3.1.189/26` | ...                  | ...                  | `10.3.1.190/26`       |
| `dns1.server1.tp3`   | ...                           | `10.3.1.20/25`        | ...                  | `10.3.1.126/25`       |
| `web1.server2.tp3`   | ...                           | ...                  | `10.3.1.194/28`      | `10.3.1.206/28`       |
| `johnny.client1.tp3` | `10.3.1.131-10.3.1.189/26` | ...                  | ...                  | `10.3.1.190/26`       |
| `nfs1.server2.tp3`   | ...                           | ...                  | `10.3.1.199/28`      | `10.3.1.206/28`       |
## 2. Routeur

🖥️ **VM router.tp3**

🌞 **Vous pouvez d'ores-et-déjà créer le routeur. Pour celui-ci, vous me prouverez que :**

* il a bien une IP dans les 3 réseaux, l'IP que vous avez choisie comme IP de passerelle
```
[paul@router ~]$ ip a | grep "inet "
    inet 127.0.0.1/8 scope host lo
    inet 10.0.2.15/24 brd 10.0.2.255 scope global dynamic noprefixroute enp0s3
    inet 10.3.1.126/25 brd 10.3.1.127 scope global noprefixroute enp0s8
    inet 10.3.1.190/26 brd 10.3.1.191 scope global noprefixroute enp0s9
    inet 10.3.1.206/28 brd 10.3.1.207 scope global noprefixroute enp0s10    
```
* il a un accès internet
```
[paul@router ~]$ ping 8.8.8.8
PING 8.8.8.8 (8.8.8.8) 56(84) bytes of data.
64 bytes from 8.8.8.8: icmp_seq=1 ttl=115 time=17.0 ms
64 bytes from 8.8.8.8: icmp_seq=2 ttl=115 time=27.3 ms
^C
--- 8.8.8.8 ping statistics ---
2 packets transmitted, 2 received, 0% packet loss, time 1001ms
rtt min/avg/max/mdev = 17.038/22.146/27.254/5.108 ms
```
* il a de la résolution de noms
```
[paul@router ~]$ dig | grep ";; SERVER"
;; SERVER: 192.168.1.1#53(192.168.1.1)
```
* il porte le nom router.tp3
```
[paul@router ~]$ cat /etc/hostname
router.tp3
```
* n'oubliez pas d'activer le routage sur la machine
```
[paul@router ~]$ sudo firewall-cmd --add-masquerade --zone=public
success
[paul@router ~]$ sudo firewall-cmd --add-masquerade --zone=public --permanent
success
```
# II. Services d'infra

## 1.Serveur DHCP

````
[paul@dhcp ~]$ cat /etc/hostname
dhcp.client1.tp3
````

````
[paul@marcel ~]$ cat /etc/hostname
marcel.client1.tp3
````

````
[paul@marcel ~]$ ip r s
default via 10.3.1.190 dev enp0s8
10.3.1.128/26 dev enp0s8 proto kernel scope link src 10.3.1.132
````

````
[paul@marcel ~]$ sudo cat /etc/resolv.conf
[sudo] password for paul:
; generated by /usr/sbin/dhclient-script
nameserver 1.1.1.1
````

````
[paul@marcel ~]$ ip a | grep enp0s8
2: enp0s8: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    inet 10.3.1.132/26 brd 10.3.1.191 scope global dynamic enp0s8
````

````
[paul@marcel ~]$ ping -c 1 8.8.8.8
PING 8.8.8.8 (8.8.8.8) 56(84) bytes of data.
64 bytes from 8.8.8.8: icmp_seq=1 ttl=114 time=18.3 ms

--- 8.8.8.8 ping statistics ---
1 packets transmitted, 1 received, 0% packet loss, time 0ms
rtt min/avg/max/mdev = 18.264/18.264/18.264/0.000 ms
[paul@marcel ~]$ dig ynov.com

; <<>> DiG 9.11.26-RedHat-9.11.26-4.el8_4 <<>> ynov.com
;; global options: +cmd
;; Got answer:
;; ->>HEADER<<- opcode: QUERY, status: NOERROR, id: 31036
;; flags: qr rd ra; QUERY: 1, ANSWER: 1, AUTHORITY: 0, ADDITIONAL: 1

;; OPT PSEUDOSECTION:
; EDNS: version: 0, flags:; udp: 1232
;; QUESTION SECTION:
;ynov.com.                      IN      A

;; ANSWER SECTION:
ynov.com.               10537   IN      A       92.243.16.143

;; Query time: 22 msec
;; SERVER: 1.1.1.1#53(1.1.1.1)
;; WHEN: Sun Oct 17 21:42:41 CEST 2021
;; MSG SIZE  rcvd: 53
````

````
[paul@marcel ~]$ traceroute 8.8.8.8
traceroute to 8.8.8.8 (8.8.8.8), 30 hops max, 60 byte packets
 1  _gateway (10.3.1.190)  4.298 ms  4.094 ms  3.607 ms
 2  10.0.2.2 (10.0.2.2)  3.413 ms  3.016 ms  2.824 ms
````

Tout fonctionne parfaitement !

## 2. Serveur DNS
### B. SETUP copain
🌞 Mettre en place une machine qui fera office de serveur DNS

dns1 avec l'IP 10.3.1.20/25 se trouve dans server1.
````
[paul@dns1 ~]$ ip a | grep enp0s8
2: enp0s8: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    inet 10.3.1.20/25 brd 10.3.1.127 scope global noprefixroute enp0s8
````
````
[paul@dns1 ~]$ cat /etc/hostname
dns1.server1.tp3
````
````
[paul@dns1 ~]$ ping -c 2 ynov.com
PING ynov.com (92.243.16.143) 56(84) bytes of data.
64 bytes from xvm-16-143.dc0.ghst.net (92.243.16.143): icmp_seq=1 ttl=49 time=17.4 ms
64 bytes from xvm-16-143.dc0.ghst.net (92.243.16.143): icmp_seq=2 ttl=49 time=17.6 ms

--- ynov.com ping statistics ---
2 packets transmitted, 2 received, 0% packet loss, time 1002ms
rtt min/avg/max/mdev = 17.415/17.522/17.630/0.170 ms
````
install
````
[paul@dns1 ~]$ sudo dnf install -y bind bind-utils
[...]
Nothing to do.
Complete!
````

setup de named.conf
````
[paul@dns1 ~]$ sudo cat /etc/named.conf
options {
        listen-on port 53 { any; };
        directory       "/var/named";
        dump-file       "/var/named/data/cache_dump.db";
        statistics-file "/var/named/data/named_stats.txt";
        memstatistics-file "/var/named/data/named_mem_stats.txt";
        secroots-file   "/var/named/data/named.secroots";
        recursing-file  "/var/named/data/named.recursing";
        allow-query     { any; };

        recursion no;

        dnssec-enable yes;
        dnssec-validation yes;

        managed-keys-directory "/var/named/dynamic";

        pid-file "/run/named/named.pid";
        session-keyfile "/run/named/session.key";

        include "/etc/crypto-policies/back-ends/bind.config";
};

logging {
        channel default_debug {
                file "data/named.run";
                severity dynamic;
        };
};

zone "server1.tp3" IN {
        type master;
        file "server1.tp3.forward";
        allow-update { none; };
};

zone "server2.tp3" IN {
        type master;
        file "server2.tp3.forward";
        allow-update { none; };
};
````
````
[paul@dns1 ~]$ sudo cat /var/named/server1.tp3.forward
[sudo] password for paul:
$TTL 86400
@   IN  SOA     dns1.server1.tp3. root.server1.tp3. (
         2021080804  ;Serial
         3600        ;Refresh
         1800        ;Retry
         604800      ;Expire
         86400       ;Minimum TTL
)

@         IN  NS      dns1.server1.tp3.
@         IN  A       10.3.1.20

dns1           IN  A       10.3.1.20
[paul@dns1 ~]$ sudo cat /var/named/server2.tp3.forward
$TTL 86400
@   IN  SOA     dns1.server1.tp3. root.server1.tp3. (
         2021080804  ;Serial
         3600        ;Refresh
         1800        ;Retry
         604800      ;Expire
         86400       ;Minimum TTL
)
@       IN NS   dns1.server1.tp3.
@       IN A    10.3.1.20
````

````
[paul@dns1 ~]$ sudo systemctl enable named
Created symlink /etc/systemd/system/multi-user.target.wants/named.service → /usr/lib/systemd/system/named.service.
[paul@dns1 ~]$ sudo systemctl start named

[paul@dns1 ~]$ sudo firewall-cmd --add-service=dns --permanent
success
[paul@dns1 ~]$ sudo firewall-cmd --reload
success

[paul@dns1 ~]$ sudo systemctl status named
[sudo] password for paul:
● named.service - Berkeley Internet Name Domain (DNS)
   Loaded: loaded (/usr/lib/systemd/system/named.service; enabled; vendor preset: disabled)
   Active: active (running)
   [...]
````
🌞 Tester le DNS depuis marcel.client1.tp3

Vers le bon DNS !
````
[paul@marcel ~]$ cat /etc/resolv.conf
# Generated by NetworkManager
search client1.tp3
nameserver 10.3.1.20
````
dig avec la preuve du bon serveur dns : 
````
[paul@marcel ~]$ dig ynov.com

; <<>> DiG 9.11.26-RedHat-9.11.26-4.el8_4 <<>> ynov.com
;; global options: +cmd
;; Got answer:
;; ->>HEADER<<- opcode: QUERY, status: REFUSED, id: 14631
;; flags: qr rd; QUERY: 1, ANSWER: 0, AUTHORITY: 0, ADDITIONAL: 1
;; WARNING: recursion requested but not available

;; OPT PSEUDOSECTION:
; EDNS: version: 0, flags:; udp: 1232
; COOKIE: f7fa8027bcb273ee5e74bbb1616cc55bb049a930570a2d26 (good)
;; QUESTION SECTION:
;ynov.com.                      IN      A

;; Query time: 1 msec
;; SERVER: 10.3.1.20#53(10.3.1.20)
;; WHEN: Mon Oct 18 02:52:42 CEST 2021
;; MSG SIZE  rcvd: 65

[paul@marcel ~]$ dig ynov.com | grep 10.3.1.20
;; SERVER: 10.3.1.20#53(10.3.1.20)
````
````
[paul@dhcp ~]$ sudo cat /etc/dhcp/dhcpd.conf
[sudo] password for paul:
#
# DHCP Server Configuration file.
# create new
default-lease-time 600;
max-lease-time 7200;
subnet 10.3.1.128 netmask 255.255.255.192 {
        range 10.3.1.128 10.3.1.189;
        option routers 10.3.1.190;
        option domain-name-servers 10.3.1.20;
}
````
## 3. Get deeper
### A. DNS forwarder

🌞 Affiner la configuration du DNS

````
[paul@dns1 ~]$ sudo cat /etc/named.conf | grep recursion
        recursion yes;
````
````
[paul@dns1 ~]$ cat /etc/resolv.conf
# Generated by NetworkManager
search home server1.tp3
nameserver 1.1.1.1
[paul@dns1 ~]$ ping -c 2 youtube.com
PING youtube.com (142.250.179.78) 56(84) bytes of data.
64 bytes from par21s19-in-f14.1e100.net (142.250.179.78): icmp_seq=1 ttl=112 time=17.5 ms
64 bytes from par21s19-in-f14.1e100.net (142.250.179.78): icmp_seq=2 ttl=112 time=18.0 ms

--- youtube.com ping statistics ---
2 packets transmitted, 2 received, 0% packet loss, time 1002ms
rtt min/avg/max/mdev = 17.514/17.763/18.012/0.249 ms

````
🌞 Test !
````
[paul@marcel ~]$ dig ynov.com

; <<>> DiG 9.11.26-RedHat-9.11.26-4.el8_4 <<>> ynov.com
;; global options: +cmd
;; Got answer:
;; ->>HEADER<<- opcode: QUERY, status: REFUSED, id: 20199
;; flags: qr rd; QUERY: 1, ANSWER: 0, AUTHORITY: 0, ADDITIONAL: 1
;; WARNING: recursion requested but not available

;; OPT PSEUDOSECTION:
; EDNS: version: 0, flags:; udp: 1232
; COOKIE: 2c3da4fb7ea7cdeb6832c9e7616cceeaaaed8dd1a0c8d989 (good)
;; QUESTION SECTION:
;ynov.com.                      IN      A

;; Query time: 1 msec
;; SERVER: 10.3.1.20#53(10.3.1.20)
;; WHEN: Mon Oct 18 03:33:29 CEST 2021
;; MSG SIZE  rcvd: 65
````

on a bien 10.3.1.20 en server dns


### B. On revient sur la conf du DHCP

🌞 Affiner la configuration du DHCP
il donnera désormais le bon dns
````
[paul@dhcp ~]$ sudo nano /etc/dhcp/dhcpd.conf
[paul@dhcp ~]$ sudo cat /etc/dhcp/dhcpd.conf
#
# DHCP Server Configuration file.
# create new
default-lease-time 600;
max-lease-time 7200;
authoritative;
subnet 10.3.1.128 netmask 255.255.255.192 {
        range 10.3.1.128 10.3.1.189;
        option routers 10.3.1.190;
        option domain-name-servers 10.3.1.20;
}
[paul@dhcp ~]$ sudo systemctl restart dhcpd
````

````
[paul@johnny ~]$ cat /etc/resolv.conf
; generated by /usr/sbin/dhclient-script
nameserver 10.3.1.20
[paul@johnny ~]$ cat /etc/sysconfig/network-scripts/ifcfg-enp0s8

BOOTPROTO=dhcp
NAME=enp0s8
DEVICE=enp0s8
ONBOOT=yes

[paul@johnny ~]$ sudo dhclient -r
[sudo] password for paul:
Killed old client process
[paul@johnny ~]$ sudo dhclient
````
````
[paul@johnny ~]$ ping -c 2 dns1.server1.tp3
PING dns1.server1.tp3 (10.3.1.20) 56(84) bytes of data.
64 bytes from 10.3.1.20 (10.3.1.20): icmp_seq=1 ttl=63 time=0.669 ms
64 bytes from 10.3.1.20 (10.3.1.20): icmp_seq=2 ttl=63 time=0.999 ms

--- dns1.server1.tp3 ping statistics ---
2 packets transmitted, 2 received, 0% packet loss, time 1003ms
rtt min/avg/max/mdev = 0.669/0.834/0.999/0.165 ms
````
johnny est entièrement fonctionnel et intégré au réseau.

# III. Services métier
## 1. Serveur Web

vm web1

````
[paul@web1 ~]$ hostname
web1.server2.tp3
[paul@web1 ~]$ cat /etc/sysconfig/network-scripts/ifcfg-enp0s8

BOOTPROTO=static
NAME=enp0s8
DEVICE=enp0s8
ONBOOT=yes
IPADDR=10.3.1.194
NETMASK=255.255.255.240

[paul@web1 ~]$ ip a | grep enp0s8
2: enp0s8: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    inet 10.3.1.194/28 brd 10.3.1.207 scope global noprefixroute enp0s8
[paul@web1 ~]$ sudo cat /etc/sysconfig/network
[sudo] password for paul:
# Created by anaconda
GATEWAY=10.3.1.206
````

Ensuite je vais me servir de nginx : 

````
[paul@web1 ~]$ sudo dnf install nginx
[paul@web1 ~]$ sudo systemctl enable nginx
Created symlink /etc/systemd/system/multi-user.target.wants/nginx.service → /usr/lib/systemd/system/nginx.service.
[paul@web1 ~]$ sudo systemctl start nginx
[paul@web1 ~]$ sudo firewall-cmd --add-port=80/tcp --permanent
success
[paul@web1 ~]$ sudo firewall-cmd --reload
success
[paul@web1 ~]$ sudo systemctl status nginx
● nginx.service - The nginx HTTP and reverse proxy server
   Loaded: loaded (/usr/lib/systemd/system/nginx.service; enabled; vendor preset: disabled)
   Active: active (running)
   [...]
   ````
   ````
   [paul@marcel ~]$ sudo cat /etc/hosts
127.0.0.1   localhost localhost.localdomain localhost4 localhost4.localdomain4
::1         localhost localhost.localdomain localhost6 localhost6.localdomain6
10.3.1.194      web1.server2.tp3
````
````
   [paul@marcel ~]$ curl web1.server2.tp3
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">
  <head>
    <title>Test Page for the Nginx HTTP Server on Rocky Lin[...]
````
le curl fonctionne comme il faut !

## 2. Partage de fichiers
### B. Le setup wola
🌞 Setup d'une nouvelle machine, qui sera un serveur NFS


setup nfs1 : 


````
[paul@nfs1 ~]$ hostname
nfs1.server2.tp3
[paul@nfs1 ~]$ sudo dnf -y install nfs-utils
[paul@nfs1 ~]$ sudo cat /etc/idmapd.conf | grep Domain
#Domain = server2.tp3
[paul@nfs1 ~]$ ip r s
default via 10.0.2.2 dev enp0s3 proto dhcp metric 100
default via 10.3.1.206 dev enp0s8 proto static metric 101
10.0.2.0/24 dev enp0s3 proto kernel scope link src 10.0.2.15 metric 100
10.3.1.192/28 dev enp0s8 proto kernel scope link src 10.3.1.199 metric 101

[paul@nfs1 ~]$ cd /srv/
[paul@nfs1 srv]$ sudo mkdir nfs_share
[paul@nfs1 srv]$ sudo vim /etc/exports
[paul@nfs1 srv]$ sudo cat /etc/exports
/srv/nfs_share 10.3.1.192/28(rw,no_root_squash)
[paul@nfs1 srv]$ sudo systemctl enable --now rpcbind nfs-server
Created symlink /etc/systemd/system/multi-user.target.wants/nfs-server.service → /usr/lib/systemd/system/nfs-server.service.
[paul@nfs1 srv]$ sudo firewall-cmd --add-service=nfs --permanent
success
[paul@nfs1 srv]$ sudo firewall-cmd --reload
success
[paul@nfs1 srv]$ sudo chown paul nfs_share
````

🌞 Configuration du client NFS

````
[paul@web1 ~]$ sudo dnf install nfs-utils -y
[paul@web1 ~]$ sudo cat /etc/idmapd.conf | grep server2
#Domain = server2.tp3
[paul@web1 ~]$ sudo mkdir /srv/nfs
[paul@web1 ~]$ sudo mount -t nfs nfs1.server2.tp3:/srv/nfs_share /srv/nfs
[sudo] password for paul:
PS C:\Users\Paula> ssh paul@10.3.1.194
Activate the web console with: systemctl enable --now cockpit.socket

Last login: Mon Oct 18 04:23:30 2021 from 10.3.1.193
[paul@web1 ~]$ sudo mount -t nfs nfs1.server2.tp3:/srv/nfs_share /srv/nfs
[sudo] password for paul:
[paul@web1 ~]$ df -hT
Filesystem                      Type      Size  Used Avail Use% Mounted on
devtmpfs                        devtmpfs  387M     0  387M   0% /dev
tmpfs                           tmpfs     405M     0  405M   0% /dev/shm
tmpfs                           tmpfs     405M  5.6M  400M   2% /run
tmpfs                           tmpfs     405M     0  405M   0% /sys/fs/cgroup
/dev/mapper/rl-root             xfs       6.2G  2.2G  4.1G  34% /
/dev/sda1                       xfs      1014M  241M  774M  24% /boot
tmpfs                           tmpfs      81M     0   81M   0% /run/user/1000
nfs1.server2.tp3:/srv/nfs_share nfs4      6.2G  2.1G  4.2G  34% /srv/nfs
[paul@web1 ~]$ sudo nano /etc/fstab
[paul@web1 ~]$ sudo cat /etc/fstab

#
# /etc/fstab
# Created by anaconda on Wed Sep 15 13:25:19 2021
#
# Accessible filesystems, by reference, are maintained under '/dev/disk/'.
# See man pages fstab(5), findfs(8), mount(8) and/or blkid(8) for more info.
#
# After editing this file, run 'systemctl daemon-reload' to update systemd
# units generated from this file.
#
/dev/mapper/rl-root     /                       xfs     defaults        0 0
UUID=8e13a7b7-9df8-49e8-a38d-e1e969669a77 /boot                   xfs     defaults        0 0
/dev/mapper/rl-swap     none                    swap    defaults        0 0


nfs1.server2.tp3:/srv/nfs_share /srv/nfs        nfs     defaults        0 0
````

🌞 TEEEEST

````
[paul@web1 ~]$ cd /srv/nfs/
[paul@web1 nfs]$ touch alpha
[paul@web1 nfs]$ nano alpha
[paul@web1 nfs]$ cat alpha
omega
````

````
[paul@nfs1 ~]$ cd /srv/nfs_share/
[paul@nfs1 nfs_share]$ ls
alpha
[paul@nfs1 nfs_share]$ cat alpha
omega
````
TOUT FONCTIONNEEEEEE!


# IV. Un peu de théorie: TCP et UDP

🌞 Déterminer, pour chacun de ces protocoles, s'ils sont encapsulés dans du TCP ou de l'UDP : 

tcp ou bien udp ? on va voir !

pour le SSH : 

````
[paul@web1 ~]$ sudo tcpdump -w tp3_ssh.pcap
````

On remarque que le SSH est encaspulé dans du TCP


pour le HTTP : 

````
[paul@web1 ~]$ sudo tcpdump -w tp3_http.pcap not port 22
[sudo] password for paul:
dropped privs to tcpdump
tcpdump: listening on enp0s8, link-type EN10MB (Ethernet), capture size 262144 bytes


^C16 packets captured
16 packets received by filter
0 packets dropped by kernel
[paul@web1 ~]$ sudo tcpdump -w tp3_http.pcap not port 22
dropped privs to tcpdump
tcpdump: listening on enp0s8, link-type EN10MB (Ethernet), capture size 262144 bytes
^C37 packets captured
37 packets received by filter
0 packets dropped by kernel
````
````
[paul@marcel ~]$ curl web1.server2.tp3
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">

<html xmlns="http://w
````

On remarque que le HTTP est encaspulé dans du TCP


pour le DNS : 

````
[paul@dns1 ~]$ sudo tcpdump -w tp3_dns.pcap not port 22
[sudo] password for paul:
dropped privs to tcpdump
tcpdump: listening on enp0s8, link-type EN10MB (Ethernet), capture size 262144 bytes
^C16 packets captured
16 packets received by filter
0 packets dropped by kernel
````
````
[paul@marcel ~]$ dig web1.server2.tp3

; <<>> DiG 9.11.26-RedHat-9.11.26-4.el8_4 <<>> web1.server2.tp3
;; global options: +cmd
;; Got answer:
;; ->>HEADER<<- opcode: QUERY, status: NXDOMAIN, id: 13201
;; flags: qr aa rd; QUERY: 1, ANSWER: 0, AUTHORITY: 1, ADDITIONAL: 1
;; WARNING: recursion requested but not available

;; OPT PSEUDOSECTION:
; EDNS: version: 0, flags:; udp: 1232
; COOKIE: f168b3c223fadf5564583169616cebca8e3dc5576ba2032d (good)
;; QUESTION SECTION:
;web1.server2.tp3.              IN      A

;; AUTHORITY SECTION:
server2.tp3.            86400   IN      SOA     dns1.server1.tp3. root.server1.tp3. 2021080804 3600 1800 604800 86400

;; Query time: 1 msec
;; SERVER: 10.3.1.20#53(10.3.1.20)
;; WHEN: Mon Oct 18 05:36:41 CEST 2021
;; MSG SIZE  rcvd: 127
````

On remarque que le DNS est encaspulé dans du UDP

pour le NFS : 

````
[paul@web1 srv]$ cd nfs/
[paul@web1 nfs]$ ls
alpha  tp3_dns.pcap
[paul@web1 nfs]$ touch diamant
[paul@web1 nfs]$ cd ..
[paul@web1 srv]$ touch nfs/diamant2
[paul@web1 srv]$ nano nfs/diamant2
````

````
[paul@nfs1 nfs_share]$ sudo tcpdump -w tp3_dns.pcap not port 22
[sudo] password for paul:
dropped privs to tcpdump
tcpdump: listening on enp0s3, link-type EN10MB (Ethernet), capture size 262144 bytes
^C0 packets captured
0 packets received by filter
0 packets dropped by kernel
[paul@nfs1 nfs_share]$ sudo tcpdump -w tp3_nfs.pcap not port 22
dropped privs to tcpdump
tcpdump: listening on enp0s3, link-type EN10MB (Ethernet), capture size 262144 bytes
^C0 packets captured
0 packets received by filter
0 packets dropped by kernel


[paul@nfs1 nfs_share]$ ls
alpha  diamant  tp3_dns.pcap  tp3_nfs.pcap


[paul@nfs1 nfs_share]$ sudo tcpdump -w tp3_nfs.pcap not port 22
dropped privs to tcpdump
tcpdump: listening on enp0s3, link-type EN10MB (Ethernet), capture size 262144 bytes
^C0 packets captured
0 packets received by filter
0 packets dropped by kernel
````
Je ne comprend pas pourquoi je ne reçois pas de packets alors que pourtant les fichiers se synchronisent bien dans les deux vm... Mais à en déduire je dirai TCP car c'est plus sécurisé et ça permet de bien avoir TOUTES les données.
![tp3_ssh.pcap](files/tp3_ssh.pcap)
![tp3_http.pcap](files/tp3_http.pcap)
![tp3_http.pcap](files/tp3_http.pcap)
# V. El final

🌞 Bah j'veux un schéma.

![schema](files/schemareseautp3.drawio.png)



🌞 **Vous me rendrez un 🗃️ tableau des réseaux 🗃️ qui rend compte des adresses choisies, sous la forme** :

| Nom du réseau | Adresse du réseau | Masque        | Nombre de clients possibles | Adresse passerelle | [Adresse broadcast](../../cours/lexique/README.md#adresse-de-diffusion-ou-broadcast-address) |
|---------------|-------------------|---------------|-----------------------------|--------------------|------------------------------------|
| `client1`     | `	10.3.1.128`        | `255.255.255.192` | 62                | `10.3.1.190`         | `10.3.1.191`                  |
| `server1`     | `10.3.1.0`        | `255.255.255.128` |    126                        | 10.3.1.126         | `10.3.1.127`                      |
| `server2`     | `10.3.1.192`        | `255.255.255.240` | 14                          | `10.3.1.206`         | `10.3.1.207`                      |

🌞 **Vous remplirez aussi** au fur et à mesure que vous avancez dans le TP, au fur et à mesure que vous créez des machines, **le 🗃️ tableau d'adressage 🗃️ suivant :**

| Nom machine  | Adresse de la machine dans le réseau `client1` | Adresse dans `server1` | Adresse dans `server2` | Adresse de passerelle |
|--------------|------------------------------------------------|------------------------|------------------------|-----------------------|
| `router.tp3`         | `10.3.1.190/26`               | `10.3.1.126/25`      | `10.3.1.206/28`      | Carte NAT             |
| `dhcp.client1.tp3`   | `10.3.1.130/26`               | x                | x                | `10.3.1.190/26`       |
| `marcel.client1.tp3` | `10.3.1.131-10.3.1.189/26` |x                  | x                | `10.3.1.190/26`       |
| `dns1.server1.tp3`   | x                          | `10.3.1.20/25`        | x                 | `10.3.1.126/25`       |
| `web1.server2.tp3`   | x                          | x                | `10.3.1.194/28`      | `10.3.1.206/28`       |
| `johnny.client1.tp3` | `10.3.1.131-10.3.1.189/26` | x               | x               | `10.3.1.190/26`       |
| `nfs1.server2.tp3`   | x                           | x                  | `10.3.1.199/28`      | `10.3.1.206/28`       |


![dhcpd.conf](files/dhcpd.conf)


