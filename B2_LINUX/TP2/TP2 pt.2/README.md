# ***TP2 pt.2-LINUX***

# Sommaire

- [TP2 pt. 2 : Maintien en condition opérationnelle](#tp2-pt-2--maintien-en-condition-opérationnelle)
- [Sommaire](#sommaire)
- [0. Prérequis](#0-prérequis)
  - [Checklist](#checklist)
- [I. Monitoring](#i-monitoring)
  - [1. Le concept](#1-le-concept)
  - [2. Setup](#2-setup)
- [II. Backup](#ii-backup)
  - [1. Intwo bwo](#1-intwo-bwo)
  - [2. Partage NFS](#2-partage-nfs)
  - [3. Backup de fichiers](#3-backup-de-fichiers)
  - [4. Unité de service](#4-unité-de-service)
    - [A. Unité de service](#a-unité-de-service)
    - [B. Timer](#b-timer)
    - [C. Contexte](#c-contexte)
  - [5. Backup de base de données](#5-backup-de-base-de-données)
  - [6. Petit point sur la backup](#6-petit-point-sur-la-backup)
- [III. Reverse Proxy](#iii-reverse-proxy)
  - [1. Introooooo](#1-introooooo)
  - [2. Setup simple](#2-setup-simple)
  - [3. Bonus HTTPS](#3-bonus-https)
- [IV. Firewalling](#iv-firewalling)
  - [1. Présentation de la syntaxe](#1-présentation-de-la-syntaxe)
  - [2. Mise en place](#2-mise-en-place)
    - [A. Base de données](#a-base-de-données)
    - [B. Serveur Web](#b-serveur-web)
    - [C. Serveur de backup](#c-serveur-de-backup)
    - [D. Reverse Proxy](#d-reverse-proxy)
    - [E. Tableau récap](#e-tableau-récap)

# I. Monitoring
## 1. Le concept
## 2. Setup
🌞 Setup Netdata
Je ferai ses commandes sur toutes les machines.
```
[paul@db ~]$ sudo su -
[sudo] password for paul:
[root@db ~]# bash <(curl -Ss https://my-netdata.io/kickstart-static64.sh)
[root@db ~]$ exit
```
🌞 Manipulation du service Netdata
```
[paul@web ~]$ systemctl status netdata
● netdata.service - Real time performance monitoring
   Loaded: loaded (/usr/lib/systemd/system/netdata.service; enabled; vendor preset: disabled)
   Active: active (running) since Mon 2021-10-11 15:47:58 CEST; 6min ago
[...]
```
```
[paul@db ~]$ systemctl status netdata
● netdata.service - Real time performance monitoring
   Loaded: loaded (/usr/lib/systemd/system/netdata.service; enabled; vendor preset: disabled)
   Active: active (running) since Mon 2021-10-11 15:48:01 CEST; 10min ago
   [...]
```
```
[paul@web ~]$ sudo ss -alpnt | grep netdata
[sudo] password for paul:
LISTEN 0      128        127.0.0.1:8125       0.0.0.0:*    users:(("netdata",pid=2189,fd=35))

LISTEN 0      128          0.0.0.0:19999      0.0.0.0:*    users:(("netdata",pid=2189,fd=5))

LISTEN 0      128            [::1]:8125          [::]:*    users:(("netdata",pid=2189,fd=34))

LISTEN 0      128             [::]:19999         [::]:*    users:(("netdata",pid=2189,fd=6))
```
```
[paul@db ~]$ sudo ss -alpnt | grep netdata
LISTEN 0      128        127.0.0.1:8125       0.0.0.0:*    users:(("netdata",pid=1980,fd=35))
LISTEN 0      128          0.0.0.0:19999      0.0.0.0:*    users:(("netdata",pid=1980,fd=5))
LISTEN 0      128            [::1]:8125          [::]:*    users:(("netdata",pid=1980,fd=34))
LISTEN 0      128             [::]:19999         [::]:*    users:(("netdata",pid=1980,fd=6))
```
écoutes sur les ports 8125 et 19999.
```
[paul@web ~]$ sudo firewall-cmd --add-port=8125/tcp --permanent
[sudo] password for paul:
success
[paul@web ~]$ sudo firewall-cmd --add-port=19999/tcp --permanent
success
[paul@web ~]$ sudo firewall-cmd --reload
success
[paul@web ~]$
```
```
[paul@db ~]$ sudo firewall-cmd --add-port=8125/tcp --permanent
[sudo] password for paul:
success
[paul@db ~]$ sudo firewall-cmd --add-port=19999/tcp --permanent
success
[paul@db ~]$ sudo firewall-cmd --reload
success
```
```
[paul@web ~]$ curl 10.102.1.11:19999
<!doctype html><html lang="en"><head><title>netdata dashboard</title><meta name="application-name"
```
```
[paul@db ~]$ curl 10.102.1.11:19999
<!doctype html><html lang="en"><head><title>netdata 
```
🌞 Setup Alerting
```
[paul@web ~]$ sudo /opt/netdata/etc/netdata/edit-config health_alarm_notify.conf
Editing '/opt/netdata/etc/netdata/health_alarm_notify.conf' ...
```
```
[netdata@web ~]$ export NETDATA_ALARM_NOTIFY_DEBUG=1
[netdata@web ~]$ /opt/netdata/usr/libexec/netdata/plugins.d/alarm-notify.sh test

# SENDING TEST WARNING ALARM TO ROLE: sysadmin
2021-10-11 17:28:19: alarm-notify.sh: INFO: sent discord notification for: web.tp2.linux test.chart.test_alarm is WARNING to 'botlinux-tp2'
# OK

# SENDING TEST CRITICAL ALARM TO ROLE: sysadmin
2021-10-11 17:28:20: alarm-notify.sh: INFO: sent discord notification for: web.tp2.linux test.chart.test_alarm is CRITICAL to 'botlinux-tp2'
# OK

# SENDING TEST CLEAR ALARM TO ROLE: sysadmin
2021-10-11 17:28:21: alarm-notify.sh: INFO: sent discord notification for: web.tp2.linux test.chart.test_alarm is CLEAR to 'botlinux-tp2'
# OK
```
je reçois les messages discord.

🌞 Config alerting
```
[paul@web netdata]$ sudo ./edit-config health.d/ram-usage.conf
Editing '/opt/netdata/etc/netdata/health.d/ram-usage.conf' ...
```
```
sudo sed -i 's/curl=""/curl="\/opt\/netdata\/bin\/curl -k"/' /opt/netdata/etc/netdata/health_alarm_notify.conf
   99  sudo /opt/netdata/etc/netdata/edit-config health_alarm_notify.conf
  100  /opt/netdata
  101  cd /opt/netdata
  102  ls
  103  pwd
  104  ls -al
  105  cd etc/netdata/
  106  pwd
  107  sudo touch health.d/ram-usage.conf
  108  sudo ./edit-config health.d/ram-usage.conf
  109  sudo netdatacli reload-health
  110  sudo killall -USR2 netdata
  118  sudo dnf install stress
  120  stress --vm 2
  127  history
  ````
  ```
  [paul@db netdata]$ sudo cat ./edit-config health.d/ram-usage.conf
  
   alarm: ram_usage
    on: system.ram
lookup: average -1m percentage of used
 units: %
 every: 1m
  warn: $this > 50
  crit: $this > 52
  info: The percentage of RAM being used by the system.
  ```
  pour db : 
  ```
  [paul@db netdata]$ sudo dnf install -y epel-release
  ```
j'ai bien reçu les alertes discord.
```
netdata on web.tp2.linux
BOT
 — Aujourd’hui à 15:07
web.tp2.linux needs attention, system.ram (ram), ram usage = 49.2%
[15:07]
web.tp2.linux needs attention, netdata.execution_time_of_phpfpm_local (go.d), go.d job last collected secs = 00:00:29 ago
web.tp2.linux is critical, system.ram (ram), ram usage = 55.2%
```


# II. Backup

| Machine         | IP            | Service                 | Port ouvert | IPs autorisées |
|-----------------|---------------|-------------------------|-------------|---------------|
| `web.tp2.linux` | `10.102.1.11` | Serveur Web             | 80 , 8125 et 19999           |   x         |
| `db.tp2.linux`  | `10.102.1.12` | Serveur Base de Données | 80 , 3306 , 19999           | 10.102.1.11             |
| `backup.tp2.linux`  | `10.102.1.13` | Serveur de Backup (NFS) | 80 , 8125 , 19999           | ?             |
## 1. Intwo bwo
```
[paul@backup ~]$ sudo dnf -y install nfs-utils
Last metadata expiration check: 2:42:18 ago on Tue 12 Oct 2021 02:34:30 PM CEST.[...]
```
```
[paul@backup ~]$ vi /etc/idmapd.conf
[paul@backup ~]$ sudo vi /etc/idmapd.conf
[paul@backup ~]$ [paul@backup ~]$ cd
[paul@backup ~]$ vi /etc/exports
[paul@backup ~]$ [paul@backup ~]$ cd
[paul@backup netdata]$ sudo vi /etc/exports
[sudo] password for paul:
[paul@backup netdata]$ cat /etc/exports
/srv/backup/web.tp2.linux 10.102.1.11/24(rw,no_root_squash)
/srv/backup/db.tp2.linux 10.102.1.12/24(rw,no_root_squash)
[paul@backup backup]$ sudo mkdir db.tp2.linux
[paul@backup backup]$ ls
db.tp2.linux  web.tp2.linux
[paul@backup ~]$ sudo systemctl enable --now rpcbind nfs-server
Created symlink /etc/systemd/system/multi-user.target.wants/nfs-server.service → /usr/lib/systemd/system/nfs-server.service.
[paul@backup ~]$ sudo firewall-cmd --add-service=nfs
success
[paul@backup ~]$ sudo firewall-cmd --add-service={nfs3,mountd,rpc-bind}
success
[paul@backup ~]$ sudo firewall-cmd --runtime-to-permanent
success
[paul@backup ~]$
[root@web ~]# sudo mkdir /srv/backup
[root@web ~]# sudo nano /etc/hosts
```

Bon c'est le moment où j'avais effacé mes vm s  en trop et j'ai dû repartir de zéro , j'ai eu plein de problème donc je me suis arrêté là ...

## 2. Partage NFS
## 3. Backup de fichiers
🌟 **BONUS**
## 4. Unité de service
### A. Unité de service
### B. Timer
### C. Contexte
## 5. Backup de base de données
## 6. Petit point sur la backup
# III. Reverse Proxy

## 1. Introooooo
## 2. Setup simple
## 3. Bonus HTTPS
🌟 **Générer la clé et le certificat pour le chiffrement**
🌟 **Modifier la conf de NGINX**
🌟 **TEST**
# IV. Firewalling
## 1. Présentation de la syntaxe
## 2. Mise en place

### A. Base de données
### B. Serveur Web
### C. Serveur de backup
### D. Reverse Proxy
### E. Tableau récap
