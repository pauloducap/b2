# TP2 pt. 1-LINUX

# Sommaire

- [TP2 pt. 1 : Gestion de service](#tp2-pt-1--gestion-de-service)
- [Sommaire](#sommaire)
- [0. Prérequis](#0-prérequis)
  - [Checklist](#checklist)
- [I. Un premier serveur web](#i-un-premier-serveur-web)
  - [1. Installation](#1-installation)
  - [2. Avancer vers la maîtrise du service](#2-avancer-vers-la-maîtrise-du-service)
- [II. Une stack web plus avancée](#ii-une-stack-web-plus-avancée)
  - [1. Intro](#1-intro)
  - [2. Setup](#2-setup)
    - [A. Serveur Web et NextCloud](#a-serveur-web-et-nextcloud)
    - [B. Base de données](#b-base-de-données)
    - [C. Finaliser l'installation de NextCloud](#c-finaliser-linstallation-de-nextcloud)

# I. Un premier serveur web

## 1. Installation

🌞 **Installer le serveur Apache**
```
[paul@web ~]$ sudo yum install httpd
Rocky Linux 8 - AppStream                                                               9.4 kB/s | 4.8 kB     00:00
Rocky Linux 8 - AppStream
[...]
  mod_http2-1.15.7-3.module+el8.4.0+553+7a69454b.x86_64
  rocky-logos-httpd-84.5-8.el8.noarch
```
```
[paul@web ~]$ sudo vim /etc/httpd/conf/httpd.conf
[paul@web ~]$ sudo cat /etc/httpd/conf/httpd.conf

ServerRoot "/etc/httpd"

Listen 80

Include conf.modules.d/*.conf

User apache
Group apache


ServerAdmin root@localhost


<Directory />
    AllowOverride none
    Require all denied
</Directory>


DocumentRoot "/var/www/html"

<Directory "/var/www">
    AllowOverride None
    Require all granted
</Directory>

<Directory "/var/www/html">
    Options Indexes FollowSymLinks

    AllowOverride None

    Require all granted
</Directory>

<IfModule dir_module>
    DirectoryIndex index.html
</IfModule>

<Files ".ht*">
    Require all denied
</Files>

ErrorLog "logs/error_log"

LogLevel warn

<IfModule log_config_module>
    LogFormat "%h %l %u %t \"%r\" %>s %b \"%{Referer}i\" \"%{User-Agent}i\"" combined
    LogFormat "%h %l %u %t \"%r\" %>s %b" common

    <IfModule logio_module>
      LogFormat "%h %l %u %t \"%r\" %>s %b \"%{Referer}i\" \"%{User-Agent}i\" %I %O" combinedio
    </IfModule>


    CustomLog "logs/access_log" combined
</IfModule>

<IfModule alias_module>


    ScriptAlias /cgi-bin/ "/var/www/cgi-bin/"

</IfModule>

<Directory "/var/www/cgi-bin">
    AllowOverride None
    Options None
    Require all granted
</Directory>

<IfModule mime_module>
    TypesConfig /etc/mime.types

    AddType application/x-compress .Z
    AddType application/x-gzip .gz .tgz



    AddType text/html .shtml
    AddOutputFilter INCLUDES .shtml
</IfModule>

AddDefaultCharset UTF-8

<IfModule mime_magic_module>
    MIMEMagicFile conf/magic
</IfModule>


EnableSendfile on

IncludeOptional conf.d/*.conf
Complete!
```

🌞 **Démarrer le service Apache**
```
[paul@web ~]$ sudo systemctl start httpd.service
[sudo] password for paul:
[paul@web ~]$ sudo systemctl enable httpd.service
Created symlink /etc/systemd/system/multi-user.target.wants/httpd.service → /usr/lib/systemd/system/httpd.service.
[paul@web ~]$ sudo firewall-cmd --add-port=80/tcp --permanent
success
[paul@web ~]$ sudo firewall-cmd --reload
success
```
```
[paul@web ~]$ sudo ss -alnpt
State           Recv-Q          Send-Q                   Local Address:Port                   Peer Address:Port         Process
LISTEN          0               128                            0.0.0.0:22                          0.0.0.0:*             users:(("sshd",pid=749,fd=5))
LISTEN          0               128                                  *:80                                *:*             users:(("httpd",pid=2055,fd=4),("httpd",pid=2054,fd=4),("httpd",pid=2053,fd=4),("httpd",pid=2051,fd=4))
LISTEN          0               128                               [::]:22                             [::]:*             users:(("sshd",pid=749,fd=7))
[paul@web ~]$
```
🌞 **TEST**
```
[paul@web ~]$ sudo systemctl status httpd
[sudo] password for paul:
● httpd.service - The Apache HTTP Server
   Loaded: loaded (/usr/lib/systemd/system/httpd.service; enabled; vendor preset: disabled)
   Active: active (running) since Wed 2021-09-29 16:40:48 CEST; 26min ago
     Docs: man:httpd.service(8)
 Main PID: 2051 (httpd)
   Status: "Running, listening on: port 80"
    Tasks: 213 (limit: 4947)
   Memory: 24.9M
   CGroup: /system.slice/httpd.service
           ├─2051 /usr/sbin/httpd -DFOREGROUND
           ├─2052 /usr/sbin/httpd -DFOREGROUND
           ├─2053 /usr/sbin/httpd -DFOREGROUND
           ├─2054 /usr/sbin/httpd -DFOREGROUND
           └─2055 /usr/sbin/httpd -DFOREGROUND

Sep 29 16:40:48 web.tp2.linux systemd[1]: Starting The Apache HTTP Server...
Sep 29 16:40:48 web.tp2.linux systemd[1]: Started The Apache HTTP Server.
Sep 29 16:40:48 web.tp2.linux httpd[2051]: Server configured, listening on: port 80
```
```
[paul@web ~]$ sudo systemctl is-enabled httpd
enabled
```
```
[paul@web ~]$ curl localhost
<!doctype html>
<html>
  <head>
    <meta charset='utf-8'>
    <meta name='viewport' content='width=device-width, initial-scale=1'>
    <title>HTTP Server Test Page powered by: Rocky Linux</title>
    <style type="text/css">
      /*<![CDATA[*/

      html {
[...]
     <a href="https://apache.org">Apache&trade;</a> is a registered trademark of <a href="https://apache.org">the Apache Software Foundation</a> in the United States and/or other countries.<br />
      <a href="https://nginx.org">NGINX&trade;</a> is a registered trademark of <a href="https://">F5 Networks, Inc.</a>.
      </footer>

  </body>
</html>
```

```
PS C:\Users\Paula> curl 10.102.1.11:80
<!doctype html>
<html>
  <head>
    <meta charset='utf-8'>
    <meta name='viewport' content='width=device-width, initial-scale=1'>
    <title>HTTP Server Test Page powered by: Rocky Linux</title>
    <style type="text/css">
      /*<![CDATA[*/

      html {
        height: 100%;
        width: 100%;
      }
        body {
  background: rgb(20,72,50);
  background: -moz-linear-gradient(180deg, rgba(20,72,50,1) 30%, rgba(0,0,0,1) 90%)  ;
  background: -webkit-linear-gradient(180deg, rgba(20,72,50,1) 30%, rgba(0,0,0,1) 90%) ;
  background: linear-gradient(180deg, rgba(20,72,50,1) 30%, rgba(0,0,0,1) 90%);
  background-repeat: no-repeat;
  [...]
          choice and edit the <code>root</code> configuration directive
        in <code>/etc/nginx/nginx.conf</code>.</p>

        <div id="logos">
          <a href="https://rockylinux.org/" id="rocky-poweredby"><img src= "icons/poweredby.png" alt="[ Powered by Rocky Linux ]" /></a> <!-- Rocky -->
          <img src="poweredby.png" /> <!-- webserver -->
        </div>
      </div>
      </div>

      <footer class="col-sm-12">
      <a href="https://apache.org">Apache&trade;</a> is a registered trademark of <a href="https://apache.org">the Apache Software Foundation</a> in the United States and/or other countries.<br />
      <a href="https://nginx.org">NGINX&trade;</a> is a registered trademark of <a href="https://">F5 Networks, Inc.</a>.
      </footer>

  </body>
</html>
```
## 2. Avancer vers la maîtrise du service
🌞 **Le service Apache...**
```
[paul@web ~]$ sudo systemctl enable httpd
[paul@web ~]$ chkconfig httpd on
Note: Forwarding request to 'systemctl enable httpd.service'.
==== AUTHENTICATING FOR org.freedesktop.systemd1.manage-unit-files ====
Authentication is required to manage system service or unit files.
Authenticating as: paul
Password:
==== AUTHENTICATION COMPLETE ====
==== AUTHENTICATING FOR org.freedesktop.systemd1.reload-daemon ====
Authentication is required to reload the systemd state.
Authenticating as: paul
Password:
==== AUTHENTICATION COMPLETE ====
```
```[paul@web ~]$ systemctl status httpd
● httpd.service - The Apache HTTP Server
   Loaded: loaded (/usr/lib/systemd/system/httpd.service; enabled;
   [...]
   ``` 
   on remarque que le service est enable .
   
   ```
   [paul@web ~]$ cat /usr/lib/systemd/system/httpd.service
# See httpd.service(8) for more information on using the httpd service.

# Modifying this file in-place is not recommended, because changes
# will be overwritten during package upgrades.  To customize the
# behaviour, run "systemctl edit httpd" to create an override unit.

# For example, to pass additional options (such as -D definitions) to
# the httpd binary at startup, create an override unit (as is done by
# systemctl edit) and enter the following:

#       [Service]
#       Environment=OPTIONS=-DMY_DEFINE

[Unit]
Description=The Apache HTTP Server
Wants=httpd-init.service
After=network.target remote-fs.target nss-lookup.target httpd-init.service
Documentation=man:httpd.service(8)

[Service]
Type=notify
Environment=LANG=C

ExecStart=/usr/sbin/httpd $OPTIONS -DFOREGROUND
ExecReload=/usr/sbin/httpd $OPTIONS -k graceful
# Send SIGWINCH for graceful stop
KillSignal=SIGWINCH
KillMode=mixed
PrivateTmp=true

[Install]
WantedBy=multi-user.target
```
🌞 **Déterminer sous quel utilisateur tourne le processus Apache**
```
[paul@web ~]$ cat /etc/httpd/conf/httpd.conf

ServerRoot "/etc/httpd"

Listen 80

Include conf.modules.d/*.conf

User apache
Group apache


ServerAdmin root@localhost
[...]
```

on remarque que le user en question est apache.
```
[paul@web ~]$ ps -ef
UID          PID    PPID  C STIME TTY          TIME CMD
[...]
root         767       1  0 14:36 ?        00:00:00 /usr/sbin/crond -n
apache       781     756  0 14:36 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
apache       782     756  0 14:36 ?        00:00:01 /usr/sbin/httpd -DFOREGROUND
apache       783     756  0 14:36 ?        00:00:01 /usr/sbin/httpd -DFOREGROUND
apache       784     756  0 14:36 ?        00:00:01 /usr/sbin/httpd -DFOREGROUND
paul        1534       1  0 14:36 ?        00:00:00 /usr/lib/systemd/systemd --user
```
apache tourne sous l'utilisateur apache.
```
[paul@web ~]$ ls -al /usr/share/testpage/
total 12
drwxr-xr-x.  2 root root   24 Sep 29 16:34 .
drwxr-xr-x. 91 root root 4096 Sep 29 16:34 ..
-rw-r--r--.  1 root root 7621 Jun 11 17:23 index.html
```
Tous les utilisateurs peuvent lire le contenu.
🌞 **Changer l'utilisateur utilisé par Apache**
```
[paul@web ~]$ cat /etc/passwd | grep "apache"
apache:x:48:48:Apache:/usr/share/httpd:/sbin/nologin
```
```
[paul@web ~]$ sudo useradd Adminapache --shell /sbin/nologin --home /usr/share/httpd
[sudo] password for paul:
useradd: warning: the home directory already exists.
Not copying any file from skel directory into it.
[paul@web ~]$ cat /etc/passwd | grep "apache"
apache:x:48:48:Apache:/usr/share/httpd:/sbin/nologin
Adminapache:x:1001:1001::/usr/share/httpd:/sbin/nologin
```
```
[paul@web ~]$ sudo nano /etc/httpd/conf/httpd.conf
[sudo] password for paul:
[paul@web ~]$ sudo nano /etc/httpd/conf/httpd.conf
[paul@web ~]$ cat /etc/httpd/conf/httpd.conf | grep "apache"
User Adminapache
Group Adminapache
```
```
[paul@web ~]$ ps -ef
UID          PID    PPID  C STIME TTY          TIME CMD
root        1852       1  0 16:17 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
Adminap+    1854    1852  0 16:17 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
Adminap+    1855    1852  0 16:17 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
Adminap+    1856    1852  0 16:17 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
Adminap+    1857    1852  0 16:17 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
paul        2071    1579  0 16:17 pts/0    00:00:00 ps -ef
```
🌞 **Faites en sorte que Apache tourne sur un autre port**

actuellement il tourne sur le port 80 donc on va le changer et mettre 777 par exemple.
```
[paul@web ~]$ cat /etc/httpd/conf/httpd.conf | grep "777"
Listen 777
```
```
[paul@web ~]$ sudo firewall-cmd --remove-port=80/tcp --permanent
success
[paul@web ~]$ sudo firewall-cmd --add-port=777/tcp --permanent
success
[paul@web ~]$ sudo firewall-cmd --reload
success
[paul@web ~]$ systemctl restart httpd
==== AUTHENTICATING FOR org.freedesktop.systemd1.manage-units ====
Authentication is required to restart 'httpd.service'.
Authenticating as: paul
Password:
==== AUTHENTICATION COMPLETE ====
```
```
[paul@web ~]$ sudo ss -alpnt
State          Recv-Q          Send-Q                   Local Address:Port                   Peer Address:Port
Process
LISTEN         0               128                            0.0.0.0:22                          0.0.0.0:*
 users:(("sshd",pid=752,fd=5))
LISTEN         0               128                                  *:777                               *:*
 users:(("httpd",pid=2351,fd=4),("httpd",pid=2350,fd=4),("httpd",pid=2349,fd=4),("httpd",pid=2346,fd=4))
LISTEN         0               128                               [::]:22                             [::]:*
[paul@web ~]$
```
```
[paul@web ~]$ curl 10.102.1.11:777
<!doctype html>
<html>
  <head>
    <meta charset='utf-8'>
    <meta name='viewport' content='width=device-width, initial-scale=1'>
    <title>HTTP Server Test Page powered by: Rocky Linux</title>
    <style type="text/css">
      /*<![CDATA[*/

      html {
        height: 100%;
        width: 100%;
      }
        body {
  background: rgb(20,72,50);
  background: -moz-linea
  [...]
  ```
```
PS C:\Users\Paula> curl 10.102.1.11:777
<!doctype html>
<html>
  <head>
    <meta charset='utf-8'>
    <meta name='viewport' content='width=device-width, initial-scale=1'>
    <title>HTTP Server Test Page powered by: Rocky Linux</title>
    <style type="text/css">
      /*<![CDATA[*/

      html {
        height: 100%;
        width: 100%;
        [...]
```
# II. Une stack web plus avancée
## 1. Intro
## 2. Setup
### A. Serveur Web et NextCloud
🌞 **Install du serveur Web et de NextCloud sur `web.tp2.linux` **


J'ai pas mal de commandes inutiles ou répétées.

```
  102  sudo dnf install httpd php
  103  sudo mkdir /etc/httpd/sites-available
  104  sudo mkdir /etc/httpd/sites-enabled
  105  sudo mkdir /var/www/sub-domains/
  106  sudo vi /etc/httpd/sites-available/com.wiki.www
  107  sudo vim /etc/httpd/sites-available/linux.tp2.web
  108  sudo cd /etc/httpd/sites-available/
  109  ls
  110  sudo ls /etc/httpd/sites-available/
  111  sudo rmdir /etc/httpd/sites-available/linux.tp2.web
  112  sudo rm /etc/httpd/sites-available/linux.tp2.web
  113  sudo ls /etc/httpd/sites-available/
  114  sudo vim /etc/httpd/sites-available/linux.tp2.web
  115  cd
  116  sudo vim /etc/httpd/sites-available/linux.tp2.web
  117  sudo rm /etc/httpd/sites-available/linux.tp2.web.swp
  118  sudo rm /etc/httpd/sites-available/.linux.tp2.web.swp
  119  sudo ls /etc/httpd/sites-available/
  120  sudo vim /etc/httpd/sites-available/linux.tp2.web
  121  sudo ln -s /etc/httpd/sites-available/com.yourdomain.nextcloud /etc/httpd/sites-enabled/
  122  sudo ln -s /etc/httpd/sites-available/linux.tp2.web /etc/httpd/sites-enabled/
  123  sudo mkdir -p /var/www/sub-domains/linux.tp2.web/html
  124  cd /usr/share/zoneinfo
  125  ls
  126  sudo vim /etc/opt/remi/php74/php.ini
  127  sudo wget https://download.nextcloud.com/server/releases/nextcloud-21.0.1.zip
  128  sudo unzip nextcloud-21.0.1.zip
  129  cd ..
  130  cd
  131  cd nextcloud
  132  cd /root/nextcloud
  133  sudo cd /root/nextcloud
  134  sudo cd /root
  135  ls
  136  sudo cd /paul
  137  cd nextcloud
  138  cd /usr/share/zoneinfo
  139  ls
  140  sudo rm nextcloud-21.0.1.zip
  141  ls
  142  cd
  143  sudo wget https://download.nextcloud.com/server/releases/nextcloud-21.0.1.zip
  144  cd nextcloud
  145  ls
  146  unzip nextcloud-21.0.1.zip
  147  cd nextcloud
  148  cp -Rf * /var/www/sub-domains/linux.tp2.web/html/
  149  sudo cp -Rf * /var/www/sub-domains/linux.tp2.web/html/
  150  sudo chown -Rf apache.apache /var/www/sub-domains/linux.tp2.web/html
  151  sudo mv /var/www/sub-domains/linux.tp2.web/html/data /var/www/sub-domains/linux.tp2.web/
  152  ls
  153  cd var
  154  cd
  155  cd var
  156  sudo mv /var/www/sub-domains/linux.tp2.web/html/data /var/www/sub-domains/linux.tp2.web/
  157  cd ./var/www
  158  sudo cd ./var/www
  159  cd nextcloud
  160  ls
  161  cd ./sub-domains
  162  cd
  163  sudo nano /etc/httpd/conf/httpd.conf
  164  ls /var/www/sub-domains/linux.tp2.web/ -larth
  165  systemctl restart httpd
  166  history
  167  systemctl status httpd
  168  curl localhost
  169  history
  ```
### B. Base de données

🌞 **Install de MariaDB sur `db.tp2.linux`**

```

    2  sudo dnf install mariadb-server
    3  systemctl enable mariadb
    4  sudo systemctl start mariadb
    5  sudo systemctl status mariadb
    6  sudo mysql_secure_installation
    7  sudo ss -naplt
    8  history
```
```
[paul@db ~]$ sudo ss -naplt
State    Recv-Q   Send-Q     Local Address:Port     Peer Address:Port  Process
LISTEN   0        128              0.0.0.0:22            0.0.0.0:*      users:(("sshd",pid=749,fd=5))
LISTEN   0        128                 [::]:22               [::]:*      users:(("sshd",pid=749,fd=7))
LISTEN   0        80                     *:3306                *:*      users:(("mysqld",pid=26132,fd=21))
```

🌞 **Préparation de la base pour NextCloud**

```sql    
[paul@db ~]$ sudo mysql -u root -p
[sudo] password for paul:
Enter password:
Welcome to the MariaDB monitor.  Commands end with ; or \g.
Your MariaDB connection id is 16
Server version: 10.3.28-MariaDB MariaDB Server

Copyright (c) 2000, 2018, Oracle, MariaDB Corporation Ab and others.

Type 'help;' or '\h' for help. Type '\c' to clear the current input statement.

MariaDB [(none)]> CREATE USER 'nextcloud'@'10.102.1.11' IDENTIFIED BY 'meow';
Query OK, 0 rows affected (0.000 sec)

MariaDB [(none)]> CREATE DATABASE IF NOT EXISTS nextcloud CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci;
Query OK, 1 row affected (0.000 sec)

MariaDB [(none)]> GRANT ALL PRIVILEGES ON nextcloud.* TO 'nextcloud'@'10.102.1.11';
Query OK, 0 rows affected (0.000 sec)

MariaDB [(none)]> FLUSH PRIVILEGES;
Query OK, 0 rows affected (0.000 sec)
```

🌞 **Exploration de la base de données**

Je vais ouvrir le port 3306.
```
[paul@db ~]$ sudo firewall-cmd --add-port=3306/tcp --permanent
[sudo] password for paul:
success
[paul@db ~]$ sudo firewall-cmd --reload
success
[paul@db ~]$ sudo systemctl restart mariadb
[paul@db ~]$ sudo systemctl status mariadb
```
```
[paul@web ~]$ sudo mysql -u nextcloud -h 10.102.1.12 -p
Enter password:
Welcome to the MariaDB monitor.  Commands end with ; or \g.
Your MariaDB connection id is 9
Server version: 10.3.28-MariaDB MariaDB Server

Copyright (c) 2000, 2018, Oracle, MariaDB Corporation Ab and others.

Type 'help;' or '\h' for help. Type '\c' to clear the current input statement.

MariaDB [(none)]>
```
Cela fonctionne !

```sql
MariaDB [(none)]> SHOW DATABASES;
+--------------------+
| Database           |
+--------------------+
| information_schema |
| nextcloud          |
+--------------------+
2 rows in set (0.001 sec)

MariaDB [(none)]> USE <DATABASE_NAME>;
ERROR 1044 (42000): Access denied for user 'nextcloud'@'10.102.1.11' to database '<DATABASE_NAME>'
MariaDB [(none)]> USE nextcloud;
Database changed
MariaDB [nextcloud]> SHOW TABLES;
Empty set (0.001 sec)

MariaDB [nextcloud]>
```

```sql
MariaDB [(none)]> SELECT user FROM mysql.user;
+-----------+
| user      |
+-----------+
| nextcloud |
| root      |
| root      |
| root      |
+-----------+
4 rows in set (0.000 sec)

```

### C. Finaliser l'installation de NextCloud

🌞 sur votre PC
Aller dans : C:\Windows\System32\drivers\etc
Je vais dans le fichier hosts avec le bloc notes sur windows et je rajoute à la fin : 
```10.102.1.11	web.tp2.linux ```

![](./files/img1.jpg)


🌞 **Exploration de la base de données**


```sql
MariaDB [(none)]> SELECT COUNT(*) FROM information_schema.tables WHERE table_schema = 'nextcloud';
+----------+
| COUNT(*) |
+----------+
|       77 |
+----------+
1 row in set (0.001 sec)
```

| Machine         | IP            | Service                 | Port ouvert | IP autorisées |
|-----------------|---------------|-------------------------|-------------|---------------|
| `web.tp2.linux` | `10.102.1.11` | Serveur Web             | 80           | X             |
|  `db.tp2.linux` | `10.102.1.12`  | Serveur Base de Données | 3306           | 10.102.1.11             |


